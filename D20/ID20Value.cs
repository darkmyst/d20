﻿using D20.ExpressionParsers.Tokenizers;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D20
{

  public interface ID20Value : IReactiveNotifyPropertyChanged, IDisposable
  {
    // 1) A magic sword of luck gives +2 luck bonus to AC. The owner of this
    // bonus is the sword. The type of bonus is Luck.
    // 2) Becoming fatigued gives a -2 fagigue penalty to Strength and Dexterity
    // The owner of this penalty is the state-of-being-fatigued. The type
    // of this penalty is Fatigued. It's worth remembering that this example
    // applies -2 to both str and dex, but it could have been -2 and -1.
    // I think therefore a given modifier should only be applied to one target
    // and any modifier owner should create and maintain a distinct modifier
    // for each target it applies to.

    // object Owner { get; }  // Who created and maintains this modifier. A character would be the owner for values intrinsic to the character such as strength
    // string Type { get; }   // The type of modifier, used when resolving stacking issues.
    IToken<string> Symbol { get; }
    double? Value { get; }
    void RefreshValue(bool forceEmit);
  }

  public interface ID20SimpleValue : ID20Value
  {
    double? Value { get; set; }
  }

  public interface ID20ComplexValue : ID20Value
  {
    string CoreExpression { get; }
    bool HasSymbol(string token);
    void AddModifier(ID20Value value);
    void RemoveModifier(string id);
  }

  public static class D20ValueExtensions
  {
    public static void EmitValueChanged(this ID20Value This, string evaluationContextKey, string symbol, double? newValue)
    {
      MessageBus.Current.SendMessage(new D20ValueChanged(evaluationContextKey, symbol, newValue));
    }
  }
}
