﻿using D20.ExpressionParsers.Parsers;
using D20.ExpressionParsers.Parsers.ExpressionNodes;
using D20.ExpressionParsers.Tokenizers;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;

namespace D20
{
  public class D20Value : ReactiveObject, ID20ComplexValue
  {
    private string _CoreExpression = null;
    private IToken<string> _Symbol = null;
    private IExpressionTreeItem _ExpressionTree = null;
    private readonly TreeParser _TreeParser = new TreeParser();
    private readonly Dictionary<string, IDisposable> _Disposables = new Dictionary<string, IDisposable>();
    private EvaluationContext _EvaluationContext;

    public D20Value(IToken<string> symbolToken, string coreExpression, EvaluationContext _EvaluationContext)
    {
      this._ExpressionTree = _TreeParser.Parse(coreExpression);
      this._CoreExpression = _ExpressionTree.ToString();
      this._Symbol = symbolToken;
      this._EvaluationContext = _EvaluationContext;

      this.CurrentExpression = this.CoreExpression;
      InitializeReactiveSubscriptions();
    }

    public D20Value(string expression, EvaluationContext _EvaluationContext)
    {
      ExpressionBinaryNode equalityExpression = (ExpressionBinaryNode)_TreeParser.Parse(expression);

      if (equalityExpression.Operator != ExpressionOperator.Equal)
        throw new ArgumentException("expression is not an Equality Expression");

      if (equalityExpression.Left.GetType() != typeof(ExpressionSymbolicLeaf))
        throw new ArgumentException("expression does not have symbol on left side of equality");

      this._ExpressionTree = equalityExpression.Right;
      this._CoreExpression = equalityExpression.Right.ToString();
      this._Symbol = ((ExpressionSymbolicLeaf)equalityExpression.Left).GetSymbolToken();
      this._EvaluationContext = _EvaluationContext;
      this.Modifiers = new ReactiveCollection<ID20Value>();
      this.CurrentExpression = this.CoreExpression;
      InitializeReactiveSubscriptions();
    }

    private void InitializeReactiveSubscriptions()
    {
      _Disposables.Add("CurrentValueChangedSubscription",
        this.WhenAny(x => x.Value, _ => _)
          .Subscribe(x => EmitValueChanged(x.Value))
      );

      _Disposables.Add("CurrentExpressionChangedSubscription",
        this.WhenAny(x => x.CurrentExpression, x => x)
            .Subscribe(_ => RefreshValue(false))
      );

      _Disposables.Add("OtherValuesChangedSubscription",
        MessageBus.Current.Listen<D20ValueChanged>()
          .Where(x => x.EvaluationContextKey == this._EvaluationContext.EvaluationContextKey)
          .Where(x => this.HasSymbol(x.Symbol) || this.HasModifier(x.Symbol))
          .Subscribe(x => RefreshValue(false))
      );
    }

    public void RefreshValue(bool forceEmit = false)
    {
      try
      {
        double? newValue = null;
        _ExpressionTree = _TreeParser.Parse(this.CurrentExpression);
        if (_ExpressionTree == null)
          newValue = null;
        else
        {
          double value;
          if (_ExpressionTree.TryEvaluate(out value, _EvaluationContext))
            newValue = value;
          else
            newValue = null;
        }

        foreach (var modifier in _Modifiers)
          newValue += modifier.Value;

        bool valueChanged = Value != newValue;
        Value = newValue;

        // If the value did not change but we want to force the emit anyway, emit
        if (forceEmit && !valueChanged)
          EmitValueChanged(Value);
      }
      catch (Exception ex)
      {
        // Handle it somehow...
      }
    }

    public IToken<string> Symbol { get { return _Symbol; } }
    public string CoreExpression { get { return _CoreExpression; } }

    double? _Value;
    public double? Value
    {
      get { return _Value; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    string _CurrentExpression;
    public string CurrentExpression
    {
      get { return _CurrentExpression; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ReactiveCollection<ID20Value> _Modifiers;
    public ReactiveCollection<ID20Value> Modifiers
    {
      get { return _Modifiers; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    public bool HasSymbol(string token)
    {
      return _ExpressionTree.FindSymbol(token) != null;
    }

    private bool HasModifier(string modifierSymbol)
    {
      return this.Modifiers.Any(x => x.Symbol.Value == modifierSymbol);
    }

    public void AddModifier(ID20Value value)
    {
      if (!this.HasModifier(value.Symbol.Value))
      {
        this._Modifiers.Add(value);
        RefreshValue();
      }
    }

    public void RemoveModifier(string modifierSymbol)
    {
      var modifier = this._Modifiers.FirstOrDefault(x => x.Symbol.Value == modifierSymbol);
      if (modifier != null)
      {
        this._Modifiers.Remove(modifier);
        RefreshValue();
      }
    }

    public void Dispose()
    {
      OnDispose();

      foreach (var disposable in _Disposables.Values)
        disposable.Dispose();

      _Disposables.Clear();
    }

    protected virtual void OnDispose()
    {
    }

    private void EmitValueChanged(double? newValue)
    {
      this.EmitValueChanged(_EvaluationContext.EvaluationContextKey, this.Symbol.Value, newValue);
    }


  }
}
