﻿using D20.ExpressionParsers.Tokenizers.TokenReaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace D20.ExpressionParsers.Tokenizers
{
  public class Tokenizer
  {
    private string _input;
    private Stack<IToken> _previousTokens = new Stack<IToken>();
    private Stack<IToken> _nextTokens = new Stack<IToken>();
    private List<ITokenReader> _tokenReaders = new List<ITokenReader>
    {
      new EqualTokenReader(),
      new AddSubtractTokenReader(),
      new MultiplyDivideTokenReader(),
      new ModulusTokenReader(),
      new ExponentTokenReader(),
      new ParenthesisTokenReader(),
      new ArgSeparatorTokenReader(),
      new NumberTokenReader(),
      new SymbolTokenReader(),
    };

    public Tokenizer(string input)
    {
      this._input = input;
    }
 
    public IToken GetNextToken()
    {
      if (this._nextTokens.Count > 0)      
      { 
        this._previousTokens.Push(this._nextTokens.Pop());
        return this._previousTokens.Peek();
      }

      if (string.IsNullOrWhiteSpace(this._input))
      {
        if (this._previousTokens.Count == 0 || this._previousTokens.Peek() != Token.End)
          this._previousTokens.Push(Token.End);

        return Token.End;
      }

      IToken token = null;
      string matchedValue = null;
      this._input = this._input.Trim();

      foreach (var tokenReader in _tokenReaders)
      { 
        if (tokenReader.TryReadToken(this._input, out matchedValue, out token))
        {
          this._input = this._input.Substring(matchedValue.Length);
          this._previousTokens.Push(token);
          return token;
        }
      }

      throw new ArgumentException("unrecognized token");
    }

    public void Revert()
    {
        this._nextTokens.Push(this._previousTokens.Pop());
    }

    public IToken Peek()
    {
      var token = GetNextToken();
      this.Revert();
      return token;
    }
  }


}
