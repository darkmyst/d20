﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace D20.ExpressionParsers.Tokenizers
{
  public interface IToken
  {
    TokenType Type { get; }
    object Value { get; }
  }

  public interface IToken<T>
  {
    TokenType Type { get; }
    T Value { get; }
  }

  public class Token
  {
    public static readonly IToken Empty = new Token<object>(null, TokenType.Empty);
    public static readonly IToken End = new Token<object>(null, TokenType.End);
  }

  [DebuggerDisplay("{Type}:{Value}")]
  public class Token<T> : Token, IToken<T>, IToken
  {
    public TokenType Type { get; protected set; }
    public T Value { get; protected set; }
    object IToken.Value 
    {
      get { return this.Value; }
    }

    public Token(T value, TokenType type)
    {
      Type = type;
      Value = value;
    }

    public override bool Equals(object obj)
    {
      if (obj == null) return false;
      
      IToken other = (IToken)obj;
      if (!other.Value.Equals(this.Value) || !other.Type.Equals(this.Type))
        return false;

      return true;
    }

    public override int GetHashCode()
    {
      int hashCode = 0;
      
      if (this.Value != null) hashCode += this.Value.GetHashCode() * 5;
      hashCode += this.Type.GetHashCode() * 10;
      return hashCode;
    }
  }

  public enum TokenType
  {
    Empty,
    Symbol,
    Equal,
    Number,
    Plus,
    Minus,
    Multiply,
    Divide,
    Exponent,
    LParen,
    RParen,
    Modulus,
    ArgSeparator,
    End,
  }
}
