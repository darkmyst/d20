﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace D20.ExpressionParsers.Tokenizers.TokenReaders
{
  public class SymbolTokenReader : ITokenReader
  {
    Regex regex = new Regex(@"\A\w(\.?[\w\d]+)*");
    public bool TryReadToken(string input, out string matchedValue, out IToken token)
    {
      var match = regex.Match(input);
      if (match.Success)
      {
        matchedValue = match.Value;
        token = new Token<string>(match.Value, TokenType.Symbol);

        return true;
      }

      matchedValue = null;
      token = null;
      return false;
    }
  }
}
