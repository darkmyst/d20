﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace D20.ExpressionParsers.Tokenizers.TokenReaders
{
  public class ParenthesisTokenReader : ITokenReader
  {
    Regex regex = new Regex(@"\A[()]");
    public bool TryReadToken(string input, out string matchedValue, out IToken token)
    {
      var match = regex.Match(input);
      if (match.Success)
      {
        matchedValue = match.Value;
        if (matchedValue == "(")
          token = new Token<object>(null, TokenType.LParen);
        else
          token = new Token<object>(null, TokenType.RParen  );

        return true;
      }

      matchedValue = null;
      token = null;
      return false;
    }
  }
}
