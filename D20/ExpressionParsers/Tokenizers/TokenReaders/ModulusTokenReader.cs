﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace D20.ExpressionParsers.Tokenizers.TokenReaders
{
  public class ModulusTokenReader : ITokenReader
  {
    Regex regex = new Regex(@"\A%");
    public bool TryReadToken(string input, out string matchedValue, out IToken token)
    {
      var match = regex.Match(input);
      if (match.Success)
      {
        matchedValue = match.Value;
        token = new Token<object>(null, TokenType.Modulus);

        return true;
      }

      matchedValue = null;
      token = null;
      return false;
    }
  }
}
