﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D20.ExpressionParsers.Tokenizers.TokenReaders
{
    internal interface ITokenReader
    {
        bool TryReadToken(string input, out string matchedValue, out IToken token);
    }
}
