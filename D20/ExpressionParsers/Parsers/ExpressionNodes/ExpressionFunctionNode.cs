using System;
using System.Linq;
using System.Collections.Generic;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{
  public class ExpressionFunctionNode : IExpressionTreeItem
  {
    public string FunctionName { get; internal set; }
    public IEnumerable<IExpressionTreeItem> Arguments { get; internal set; }

    public bool TryEvaluate(out double result, EvaluationContext context)
    {
      var resolvedArgs = Arguments.Select(x => {
        double value;
        if (!x.TryEvaluate(out value, context))
          return null;

        return (double?)value;
      });

      if (resolvedArgs.Any(x => !x.HasValue))
      {
        result = 0;
        return false;
      }

      var functionArgs = resolvedArgs.Select(x => x.Value).ToArray();
      double? functionOutput = context.ExecuteFunction(FunctionName, functionArgs);
      if (functionOutput.HasValue)
      {
        result = functionOutput.Value;
        return true;
      }

      result = 0;
      return false;
    }

    public override string ToString()
    {
      return ToString(new EvaluationContext());
    }

    public string ToString(EvaluationContext context)
    {
      return string.Format("{0}({1})", FunctionName,  string.Join(", ", Arguments.Select(x => x.ToString(context))));
    }

    public ExpressionSymbolicLeaf FindSymbol(string token)
    {
      return Arguments.Select(x => x.FindSymbol(token)).FirstOrDefault(x => x != null);
    }
  }
}