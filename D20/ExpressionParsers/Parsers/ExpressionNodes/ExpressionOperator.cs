using System;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{
  public class ExpressionOperator
  {
    protected ExpressionOperator() {}
      
    public static readonly ExpressionOperator Equal = new ExpressionOperator{ Name = "Equal", Symbol = "=", Precidence = 0 };
    public static readonly ExpressionOperator Add = new ExpressionOperator{ Name = "Add", Symbol = "+", Precidence = 1 };
    public static readonly ExpressionOperator Subtract = new ExpressionOperator{ Name = "Subtract", Symbol = "-", Precidence = 1};
    public static readonly ExpressionOperator Multiply = new ExpressionOperator{ Name = "Multiply", Symbol = "*", Precidence = 2};
    public static readonly ExpressionOperator Divide = new ExpressionOperator{ Name = "Divide", Symbol = "/", Precidence = 2};
    public static readonly ExpressionOperator Exponent = new ExpressionOperator{ Name = "Exponent", Symbol = "^", Precidence = 3, IsRightAssociative = true};
    public static readonly ExpressionOperator Negative = new ExpressionOperator{ Name = "Negative", Symbol = "-", Precidence = 1, IsUnary = true};

    
    public string Name { get; protected set; }
    public string Symbol { get; protected set; }
    public int Precidence { get; protected set; }
    public bool IsUnary { get; protected set; }
    public bool IsRightAssociative { get; protected set; }
  }
}