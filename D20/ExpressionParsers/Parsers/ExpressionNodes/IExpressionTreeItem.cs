﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{
  public interface IExpressionTreeItem
  {
    bool TryEvaluate(out double result, EvaluationContext context = null);
    string ToString(EvaluationContext context);
    ExpressionSymbolicLeaf FindSymbol(string token);
  }
}
