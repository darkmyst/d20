using System;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{
  public class ExpressionUnaryNode : IExpressionOperatorTreeItem
  {
    public ExpressionOperator Operator { get; internal set; }
    public IExpressionTreeItem Value { get; internal set; }

    public bool TryEvaluate(out double result, EvaluationContext context)
    {
      context = context ?? new EvaluationContext();
      if (Operator == ExpressionOperator.Negative)
      {
        double childResult;
        if (Value.TryEvaluate(out childResult, context))
        { 
          result = -1 * childResult;
          return true;
        }
      }

      result = 0;
      return false;
    }

    public override string ToString()
    {
      return ToString(new EvaluationContext());
    }
    
    public string ToString(EvaluationContext context)
    {
      string valueStr = Value.ToString(context);

      if (Value is IExpressionOperatorTreeItem)
        valueStr = string.Format("({0})", valueStr);

      return string.Format("{0}{1}", Operator.Symbol, valueStr);
    }

    public ExpressionSymbolicLeaf FindSymbol(string token)
    {
      return Value.FindSymbol(token);
    }
  }
  }