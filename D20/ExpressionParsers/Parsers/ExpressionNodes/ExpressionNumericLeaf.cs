using System;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{
  public class ExpressionNumericLeaf : IExpressionTreeItem
  {
    public double Value { get; internal set; }
    public bool TryEvaluate(out double result, EvaluationContext context)
    {
      result = Value;
      return true;
    }

    public override string ToString()
    {
      return ToString(new EvaluationContext());
    }

    public string ToString(EvaluationContext context)
    {
      return string.Format("{0}", Value);    
    }

    public ExpressionSymbolicLeaf FindSymbol(string token)
    {
      return null;
    }
  }
}