﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{  
  public interface IExpressionOperatorTreeItem : IExpressionTreeItem
  {
    ExpressionOperator Operator { get; }
  }
}
