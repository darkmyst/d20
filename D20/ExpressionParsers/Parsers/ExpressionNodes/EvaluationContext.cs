using System;
using System.Linq;
using System.Collections.Generic;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{
  public class EvaluationContext : IDisposable
  {
    private readonly Dictionary<string, IDisposable> _Disposables = new Dictionary<string, IDisposable>();
    public Dictionary<string, Tuple<Delegate, object>> _Functions = new Dictionary<string, Tuple<Delegate, object>>();
    private Dictionary<string, double> _SymbolValues = new Dictionary<string, double>();
    private Func<string, double?> _ResolveUnknownSymbolFunc = null;
    private EvaluationContext _ExpandedContext = null;
    private readonly string _EvaluationContextKey = Guid.NewGuid().ToString();
    public string EvaluationContextKey { get { return _EvaluationContextKey; } }


    public EvaluationContext(string evaluationContextKey = null, Dictionary<string, double> symbolValues = null, Dictionary<string, Delegate> functions = null, Func<string, double?> resolveUnknownSymbolFunc = null, EvaluationContext expandedContext = null)
    {
      if (symbolValues != null)
        _SymbolValues = symbolValues;
      if (functions != null)
        _Functions = functions.ToDictionary(x => x.Key, x => Tuple.Create(x.Value, (object)null));

      if (!string.IsNullOrWhiteSpace(evaluationContextKey))
        _EvaluationContextKey = evaluationContextKey;

      _ExpandedContext = expandedContext;
      _ResolveUnknownSymbolFunc = resolveUnknownSymbolFunc;
    }

    //public EvaluationContext(string evaluationContextKey = null, Dictionary<string, double> symbolValues = null, Dictionary<string, Tuple<Delegate, object>> functions = null, Func<string, double?> resolveUnknownSymbolFunc = null, EvaluationContext expandedContext = null)
    //{
    //  if (symbolValues != null)
    //    _SymbolValues = symbolValues;
    //  if (functions != null)
    //    _Functions = functions;

    //  if (!string.IsNullOrWhiteSpace(evaluationContextKey))
    //    _EvaluationContextKey = evaluationContextKey;

    //  _ExpandedContext = expandedContext;
    //  _ResolveUnknownSymbolFunc = resolveUnknownSymbolFunc;
    //}

    public void SetValueForSymbol(string symbol, double value)
    {
      if (!_SymbolValues.ContainsKey(symbol))
        _SymbolValues.Add(symbol, value);
      else
        _SymbolValues[symbol] = value;
    }

    public double? GetValueForSymbol(string symbol)
    {
      double? result = null;

      if (_SymbolValues.ContainsKey(symbol))
        result = _SymbolValues[symbol];

      if (!result.HasValue && _ExpandedContext != null)
        result = _ExpandedContext.GetValueForSymbol(symbol);

      if (!result.HasValue && _ResolveUnknownSymbolFunc != null)
      {
        var resolvedValue = _ResolveUnknownSymbolFunc(symbol);
        if (resolvedValue.HasValue)
          result = resolvedValue.Value;
      }

      return result;
    }

    public bool TryGetValueForSymbol(string symbol, out double resolvedValue)
    {
      if (_SymbolValues.ContainsKey(symbol))
      {
        resolvedValue = _SymbolValues[symbol];
        return true;
      }
      resolvedValue = 0;
      return false;
    }



    public void RegisterFunction(string name, Func<double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double, double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._Functions.Add(name, Tuple.Create((Delegate)method, funcInstance));
    }

    public double? ExecuteFunction(string name, params double[] args)
    {
      try
      {
        if (_Functions.ContainsKey(name))
        {
          Delegate function = _Functions[name].Item1;

          Func<double> func_D = function as Func<double>;
          if (func_D != null && args.Count() == 0)
            return func_D();

          Func<double, double> func_DD = function as Func<double, double>;
          if (func_DD != null && args.Count() == 1)
            return func_DD(args[0]);

          Func<double, double, double> func_DDD = function as Func<double, double, double>;
          if (func_DDD != null && args.Count() == 2)
            return func_DDD(args[0], args[1]);

          Func<double, double, double, double> func_DDDD = function as Func<double, double, double, double>;
          if (func_DDDD != null && args.Count() == 3)
            return func_DDDD(args[0], args[1], args[2]);

          Func<double, double, double, double, double> func_DDDDD = function as Func<double, double, double, double, double>;
          if (func_DDDDD != null && args.Count() == 4)
            return func_DDDDD(args[0], args[1], args[2], args[3]);

          Func<double, double, double, double, double, double> func_DDDDDD = function as Func<double, double, double, double, double, double>;
          if (func_DDDDDD != null && args.Count() == 5)
            return func_DDDDDD(args[0], args[1], args[2], args[3], args[4]);

          Func<double, double, double, double, double, double, double> func_DDDDDDD = function as Func<double, double, double, double, double, double, double>;
          if (func_DDDDDDD != null && args.Count() == 6)
            return func_DDDDDDD(args[0], args[1], args[2], args[3], args[4], args[5]);

          Func<double, double, double, double, double, double, double, double> func_DDDDDDDD = function as Func<double, double, double, double, double, double, double, double>;
          if (func_DDDDDDDD != null && args.Count() == 7)
            return func_DDDDDDDD(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);

          Func<double, double, double, double, double, double, double, double, double> func_DDDDDDDDD = function as Func<double, double, double, double, double, double, double, double, double>;
          if (func_DDDDDDDDD != null && args.Count() == 8)
            return func_DDDDDDDDD(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);

          Func<double, double, double, double, double, double, double, double, double, double> func_DDDDDDDDDD = function as Func<double, double, double, double, double, double, double, double, double, double>;
          if (func_DDDDDDDDDD != null && args.Count() == 9)
            return func_DDDDDDDDDD(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);

          // If all else fails...
          var funcMethod = function.Method;
          object instance = _Functions[name].Item2;
          return (double)funcMethod.Invoke(instance, args.Cast<object>().ToArray());
        }
        else if (_ExpandedContext != null)
          return _ExpandedContext.ExecuteFunction(name, args);
      }
      catch (Exception ex)
      {
        return null;
      }
      throw new InvalidOperationException("Unknown function");
    }


    public void Dispose()
    {
      OnDispose();

      foreach (var disposable in _Disposables.Values)
        disposable.Dispose();

      _Disposables.Clear();


      _Functions.Clear();
      _SymbolValues.Clear();
      _ResolveUnknownSymbolFunc = null;
      _ExpandedContext = null; // Do not dispose this!
    }

    protected virtual void OnDispose()
    {

    }
  }
}