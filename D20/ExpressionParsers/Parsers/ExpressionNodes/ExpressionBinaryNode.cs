using System;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{
  public class ExpressionBinaryNode : IExpressionOperatorTreeItem
  {
    public ExpressionOperator Operator { get; internal set; }
    public IExpressionTreeItem Left { get; internal set; }
    public IExpressionTreeItem Right { get; internal set; }


    public bool TryEvaluate(out double result, EvaluationContext context)
    {
      context = context ?? new EvaluationContext();

      double leftValue, rightValue;
      if (!Left.TryEvaluate(out leftValue, context) || !Right.TryEvaluate(out rightValue, context))
      {
        result = 0;
        return false;
      }

      if (Operator == ExpressionOperator.Add)
        result = leftValue + rightValue;
      else if (Operator == ExpressionOperator.Subtract)
        result = leftValue - rightValue;
      else if (Operator == ExpressionOperator.Multiply)
        result = leftValue * rightValue;
      else if (Operator == ExpressionOperator.Divide)
        result = leftValue / rightValue;
      else if (Operator == ExpressionOperator.Exponent)
        result = Math.Pow(leftValue, rightValue);
      else
      {
        result = 0;
        return false;
      }
      return true;
    }

    public override string ToString()
    {
      return ToString(new EvaluationContext());
    }

    public string ToString(EvaluationContext context)
    {
      string leftStr = Left.ToString(context);
      string rightStr = Right.ToString(context);

      if (Right is IExpressionOperatorTreeItem)
      {
        var rightOperator = (Right as IExpressionOperatorTreeItem).Operator;
        if (rightOperator.Precidence < this.Operator.Precidence && !rightOperator.IsUnary)
          rightStr = string.Format("({0})", rightStr);
      }
      if (Left is IExpressionOperatorTreeItem)
      {
        var leftOperator = (Left as IExpressionOperatorTreeItem).Operator;
        if (leftOperator.Precidence < this.Operator.Precidence && !leftOperator.IsUnary)
          leftStr = string.Format("({0})", leftStr);
      }
      return string.Format("{0} {1} {2}", leftStr, Operator.Symbol, rightStr);
    }


    public ExpressionSymbolicLeaf FindSymbol(string token)
    {
      var result = Left.FindSymbol(token);
      if (result != null)
        return result;

      return Right.FindSymbol(token);
    }
  }
}