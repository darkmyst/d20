using D20.ExpressionParsers.Tokenizers;
using System;

namespace D20.ExpressionParsers.Parsers.ExpressionNodes
{
  public class ExpressionSymbolicLeaf : IExpressionTreeItem
  {
    public string Symbol { get; internal set; }

    public bool TryEvaluate(out double result, EvaluationContext context)
    {
      context = context ?? new EvaluationContext();

      var symbolValue = context.GetValueForSymbol(Symbol);
      result = symbolValue ?? 0;
      return symbolValue.HasValue;
    }

    public override string ToString()
    {
      return ToString(new EvaluationContext());
    }

    public string ToString(EvaluationContext context)
    {
      double resolvedValue;
      if (context.TryGetValueForSymbol(Symbol, out resolvedValue))
        return string.Format("{0}", resolvedValue);

      return string.Format("{0}", Symbol);
    }


    public ExpressionSymbolicLeaf FindSymbol(string token)
    {
      if (this.Symbol == token)
        return this;

      return null;
    }

    public IToken<string> GetSymbolToken()
    {
      return new Token<string>(Symbol, TokenType.Symbol);
    }
  }
}