﻿using D20.ExpressionParsers.Parsers.ExpressionNodes;
using D20.ExpressionParsers.Tokenizers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace D20.ExpressionParsers.Parsers
{
  public class TreeParser
  {
    private Tokenizer _lexer;
    private Dictionary<string, Delegate> _functions = new Dictionary<string, Delegate>();
    public TreeParser()
    {
    }
    public IExpressionTreeItem Parse(string input)
    {
      this._lexer = new Tokenizer(input);

      if (this._lexer.Peek().Type == TokenType.End)
        return null;

      IExpressionTreeItem expressionValue = Equal();

      var token = this._lexer.GetNextToken();
      if (token.Type == TokenType.End)
        return expressionValue;

      throw new TreeParseException("End Expected");
    }
    
    private IExpressionTreeItem Equal()
    {
      var component1 = AddSub();

      var operators = new[] { TokenType.Equal };
      var token = this._lexer.GetNextToken();

      if (operators.Contains(token.Type))
      {
        var component2 = AddSub();

        component1 = new ExpressionBinaryNode { Left = component1, Right = component2, Operator = ExpressionOperator.Equal};

        token = this._lexer.GetNextToken();
      }

      this._lexer.Revert();
      return component1;
    }

    private IExpressionTreeItem AddSub()
    {
      var component1 = MulDiv();

      var operators = new[] { TokenType.Plus, TokenType.Minus };
      var token = this._lexer.GetNextToken();

      while (operators.Contains(token.Type))
      {
        var component2 = MulDiv();

        if (token.Type == TokenType.Plus)
          component1 = new ExpressionBinaryNode { Left = component1, Right = component2, Operator = ExpressionOperator.Add };
        else
          component1 = new ExpressionBinaryNode { Left = component1, Right = component2, Operator = ExpressionOperator.Subtract };

        token = this._lexer.GetNextToken();
      }

      this._lexer.Revert();
      return component1;
    }

    private IExpressionTreeItem MulDiv()
    {
      var component1 = Exponent();

      var operators = new[] { TokenType.Multiply, TokenType.Divide };
      var token = this._lexer.GetNextToken();

      while (operators.Contains(token.Type))
      {
        var component2 = Exponent();

        if (token.Type == TokenType.Multiply)
          component1 = new ExpressionBinaryNode { Left = component1, Right = component2, Operator = ExpressionOperator.Multiply };
        else
          component1 = new ExpressionBinaryNode { Left = component1, Right = component2, Operator = ExpressionOperator.Divide };

        token = this._lexer.GetNextToken();
      }

      this._lexer.Revert();
      return component1;
    }

    /// <summary>
    /// This is almost certainly wrong due to right-associativity
    /// </summary>
    /// <returns></returns>
    private IExpressionTreeItem Exponent()
    {
      var exponentStack = new Stack<IExpressionTreeItem>();
      exponentStack.Push(ParensOrNumber());

      var operators = new[] { TokenType.Exponent };

      var token = this._lexer.GetNextToken();
      while (operators.Contains(token.Type))
      {
        exponentStack.Push(ParensOrNumber());
        token = this._lexer.GetNextToken();
      }

      this._lexer.Revert();

      var result = exponentStack.Pop();
      while (exponentStack.Count > 0)
      {
        var expBase = exponentStack.Pop();
        result = new ExpressionBinaryNode { Left = expBase, Right = result, Operator = ExpressionOperator.Exponent };
      }
      return result;
    }

    private IExpressionTreeItem ParensOrNumber()
    {
      var token = this._lexer.GetNextToken();
      int reverse = 1;
      IExpressionTreeItem value = null;

      var signs = new[] { TokenType.Minus, TokenType.Plus };
      if (signs.Contains(token.Type))
      {
        if (token.Type == TokenType.Minus)
          reverse = -1;

        token = this._lexer.GetNextToken();
      }

      if (token.Type == TokenType.LParen)
      {
        value = AddSub();
        var expectedRparen = this._lexer.GetNextToken();
        if (expectedRparen.Type != TokenType.RParen)
          throw new TreeParseException("Unbalanced parenthesis");

      }
      else if (token.Type == TokenType.Symbol)
      {
        var functionOpenToken = this._lexer.Peek();
        if (functionOpenToken.Type == TokenType.LParen)
        {
          // drop the leading paren of a function
          this._lexer.GetNextToken();

          List<IExpressionTreeItem> args = new List<IExpressionTreeItem>();
          while (this._lexer.Peek().Type != TokenType.RParen)
          { 
            args.Add(AddSub());
            
            if (this._lexer.Peek().Type == TokenType.ArgSeparator)
              this._lexer.GetNextToken();
          }

          var expectedRparen = this._lexer.GetNextToken();
          if (expectedRparen.Type != TokenType.RParen)
            throw new TreeParseException("Unbalanced function parenthesis");

          value = new ExpressionFunctionNode { FunctionName = (string)token.Value, Arguments = args };
        }
        else
          value = new ExpressionSymbolicLeaf { Symbol = (string)token.Value };
      }
      else if (token.Type == TokenType.Number)
      {
        value = new ExpressionNumericLeaf { Value = (double)token.Value };
      }
      else
        throw new TreeParseException("Not a number");

      if (reverse == -1)
        return new ExpressionUnaryNode { Value = value, Operator = ExpressionOperator.Negative };
      else
        return value;
    }
  }

  public class TreeParseException : Exception
  {
    public TreeParseException(string message)
      :base(message)
    {

    }
  }
}
