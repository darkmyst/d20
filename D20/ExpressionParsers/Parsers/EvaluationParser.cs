﻿using D20.ExpressionParsers.Tokenizers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D20.ExpressionParsers.Parsers
{
  public class EvaluationParser
  {
    public EvaluationParser()
    {
    }
    public bool TryParse(out double result, string input)
    {
      result = 0;

      if (!new TreeParser().Parse(input).TryEvaluate(out result))
        return false;

      return true;
    }
  }
}
