﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D20
{
  public class D20ValueChanged
  {
    public string EvaluationContextKey { get; private set; }
    public string Symbol { get; private set; }
    public double? NewValue { get; private set; }

    public D20ValueChanged(string evaluationContextKey, string symbol, double? newValue)
    {
      this.EvaluationContextKey = evaluationContextKey;
      this.Symbol = symbol;
      this.NewValue = newValue;
    }
  }
}
