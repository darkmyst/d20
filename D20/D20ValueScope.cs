﻿using D20.ExpressionParsers.Parsers;
using D20.ExpressionParsers.Parsers.ExpressionNodes;
using D20.ExpressionParsers.Tokenizers;
using D20.ExpressionParsers.Tokenizers.TokenReaders;
using Json.Parsers;
using Json.Parsers.TreeNodes;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D20
{
  public class D20ValueScope : ReactiveObject, IDisposable
  {
    private readonly SymbolTokenReader symbolTokenReader = new SymbolTokenReader();
    private readonly Dictionary<string, ID20ComplexValue> registeredValues = new Dictionary<string, ID20ComplexValue>();
    private readonly Dictionary<string, ID20SimpleValue> registeredSimpleValues = new Dictionary<string, ID20SimpleValue>();
    private readonly Dictionary<string, IDisposable> _Disposables = new Dictionary<string, IDisposable>();
    private readonly JsonObjectTreeNode _DataObject = new JsonObjectTreeNode();

    private EvaluationContext _EvaluationContext = null;
    private readonly object _registrationLock = new object();

    public D20ValueScope()
    {
      _EvaluationContext = new EvaluationContext(resolveUnknownSymbolFunc: ResolveSymbolValue);
    }

    public ID20SimpleValue Create(string symbol, double value)
    {
      lock (_registrationLock)
      {
        if (registeredValues.ContainsKey(symbol) || registeredSimpleValues.ContainsKey(symbol))
          throw new ArgumentException(string.Format("Cannot register {0}, already registered", symbol));

        var simpleValue = new D20SimpleValue(symbol, value, _EvaluationContext);
        registeredSimpleValues.Add(symbol, simpleValue);
        simpleValue.RefreshValue(true); // Force emit of newly created Value
        return simpleValue;
      }
    }


    public ID20ComplexValue CreateFromExpression(string expression)
    {
      var d20Value = new D20Value(expression, _EvaluationContext);
      var symbolToken = d20Value.Symbol;
      lock (_registrationLock)
      {
        if (registeredValues.ContainsKey(symbolToken.Value) || registeredSimpleValues.ContainsKey(symbolToken.Value))
          throw new ArgumentException(string.Format("Cannot register {0}, already registered", symbolToken.Value));

        registeredValues.Add(symbolToken.Value, d20Value);
        d20Value.RefreshValue(true); // Force emit of newly created Value
        return d20Value;
      }
    }

    public ID20Value GetValue(string symbol)
    {
      if (registeredValues.ContainsKey(symbol))
        return registeredValues[symbol];

      if (registeredSimpleValues.ContainsKey(symbol))
        return registeredSimpleValues[symbol];

      return null;
    }

    public void Unregister(string symbol)
    {
      lock (_registrationLock)
      {
        if (registeredValues.ContainsKey(symbol))
        {
          registeredValues[symbol].Dispose();
          registeredValues.Remove(symbol);
        }
        else if (registeredSimpleValues.ContainsKey(symbol))
        {
          registeredSimpleValues[symbol].Dispose();
          registeredSimpleValues.Remove(symbol);
        }
      }
    }

    private double? ResolveSymbolValue(string symbol)
    {
      var valueObj = GetValue(symbol);
      if (valueObj != null)
        return valueObj.Value;
      return null;
    }



    public void Dispose()
    {
      OnDispose();

      foreach (var key in registeredValues.Keys.Union(registeredSimpleValues.Keys).Distinct().ToList())
        this.Unregister(key);

      foreach (var disposable in _Disposables.Values)
        disposable.Dispose();

      _EvaluationContext.Dispose();
      _EvaluationContext = null;
      _Disposables.Clear();
    }

    protected virtual void OnDispose()
    {
    }




    public void RegisterFunction(string name, Func<double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double, double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string name, Func<double, double, double, double, double, double, double, double, double, double> method, object funcInstance = null)
    {
      this._EvaluationContext.RegisterFunction(name, method, funcInstance);
    }

    public void RegisterFunction(string expression)
    {
      TreeParser parser = new TreeParser();
      var tree = (ExpressionBinaryNode)parser.Parse(expression);

      var functionNode = tree.Left as ExpressionFunctionNode;
      if (functionNode == null)
        throw new ArgumentException("left side of expression must be a function node");

      var functionName = functionNode.FunctionName;

      if (functionNode.Arguments.Any(x => x.GetType() != typeof(ExpressionSymbolicLeaf)))
        throw new ArgumentException("all arguments in function name must be symbols");

      var functionSymbols = functionNode.Arguments.Cast<ExpressionSymbolicLeaf>().Select(x => x.Symbol).ToArray();

      FunctionWithExpressionEvaluator.RegisterFunction(((ExpressionBinaryNode)tree).Right, _EvaluationContext, functionName, functionSymbols);
    }

    public void LoadData(string name, string json)
    {
      if (this._DataObject.Children.ContainsKey(name))
        throw new ArgumentException("dataObject already contains key");

      var jsonParser = new JsonTreeParser();
      var jsonTree = jsonParser.Parse(json);
      _DataObject.Children.Add(name, jsonTree);
    }

    public object ReadData(string path)
    {
      return this._DataObject.Read(path);
    }

    public bool HasDataKey(string key)
    {
      return this._DataObject.Children.ContainsKey(key);
    }
  }

  internal class FunctionWithExpressionEvaluator // TODO: IDisposable
  {
    private EvaluationContext _PrimaryContext;
    private string[] _ArgSymbols;
    private IExpressionTreeItem _Tree;
    private string _FunctionName;

    protected FunctionWithExpressionEvaluator(IExpressionTreeItem tree, EvaluationContext primaryContext, string functionName, string[] argSymbols)
    {
      this._PrimaryContext = primaryContext;
      this._ArgSymbols = argSymbols;
      this._Tree = tree;
      this._FunctionName = functionName;

      RegisterFunctionInternal();
    }

    public double Evaluate(double[] argValues)
    {
      var context = new EvaluationContext(expandedContext: _PrimaryContext);

      if (_ArgSymbols.Length != argValues.Length)
        throw new ArgumentException("function argument keys count does not match values count");

      foreach (var arg in _ArgSymbols.Zip(argValues, (s, v) => new { Symbol = s, Value = v }))
        context.SetValueForSymbol(arg.Symbol, arg.Value);

      double result = 0;
      if (!_Tree.TryEvaluate(out result, context))
        throw new InvalidOperationException("Function evaluation failed");

      return result;
    }

    public static void RegisterFunction(IExpressionTreeItem tree, EvaluationContext primaryContext, string functionName, string[] argSymbols)
    {
      new FunctionWithExpressionEvaluator(tree, primaryContext, functionName, argSymbols);
    }

    private void RegisterFunctionInternal()
    {
      switch (_ArgSymbols.Count())
      {
        case 0: _PrimaryContext.RegisterFunction(_FunctionName, () => Evaluate(new double[] { }), this); break;
        case 1: _PrimaryContext.RegisterFunction(_FunctionName, (x1) => Evaluate(new[] { x1 }), this); break;
        case 2: _PrimaryContext.RegisterFunction(_FunctionName, (x1, x2) => Evaluate(new[] { x1, x2 }), this); break;
        case 3: _PrimaryContext.RegisterFunction(_FunctionName, (x1, x2, x3) => Evaluate(new[] { x1, x2, x3 }), this); break;
        case 4: _PrimaryContext.RegisterFunction(_FunctionName, (x1, x2, x3, x4) => Evaluate(new[] { x1, x2, x3, x4 }), this); break;
        case 5: _PrimaryContext.RegisterFunction(_FunctionName, (x1, x2, x3, x4, x5) => Evaluate(new[] { x1, x2, x3, x4, x5 }), this); break;
        case 6: _PrimaryContext.RegisterFunction(_FunctionName, (x1, x2, x3, x4, x5, x6) => Evaluate(new[] { x1, x2, x3, x4, x5, x6 }), this); break;
        case 7: _PrimaryContext.RegisterFunction(_FunctionName, (x1, x2, x3, x4, x5, x6, x7) => Evaluate(new[] { x1, x2, x3, x4, x5, x6, x7 }), this); break;
        case 8: _PrimaryContext.RegisterFunction(_FunctionName, (x1, x2, x3, x4, x5, x6, x7, x8) => Evaluate(new[] { x1, x2, x3, x4, x5, x6, x7, x8 }), this); break;
        case 9: _PrimaryContext.RegisterFunction(_FunctionName, (x1, x2, x3, x4, x5, x6, x7, x8, x9) => Evaluate(new[] { x1, x2, x3, x4, x5, x6, x7, x8, x9 }), this); break;
        default: throw new NotImplementedException("Argument count not supported");
      }
    }
  }

}
