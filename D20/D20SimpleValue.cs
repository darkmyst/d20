﻿using D20.ExpressionParsers.Parsers.ExpressionNodes;
using D20.ExpressionParsers.Tokenizers;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D20
{
  public class D20SimpleValue : ReactiveObject, ID20SimpleValue
  {
    private readonly Dictionary<string, IDisposable> _Disposables = new Dictionary<string,IDisposable>();
    private EvaluationContext _EvaluationContext = null;
    public IToken<string> Symbol { get; protected set; }
    
    double? _Value;
    public double? Value
    {
      get { return _Value; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    public D20SimpleValue(string symbol, double value, EvaluationContext evaluationContext)
    {
      Symbol = new Token<string>(symbol, TokenType.Symbol);
      Value = value;
      _EvaluationContext = evaluationContext;

      _Disposables.Add("EmitValueChangedSubscription", this.WhenAny(x => x.Value, _ => _)
        .Subscribe(x => EmitValueChanged(x.Value)));
    }

    public void Dispose()
    {
      OnDispose();

      foreach (var disposable in _Disposables.Values)
        disposable.Dispose();
      
      _Disposables.Clear();

      _EvaluationContext = null;
      Symbol = null;
    }

    protected virtual void OnDispose()
    {      
    }

    public void RefreshValue(bool forceEmit = false)
    {
      if (forceEmit)
        EmitValueChanged(Value);
    }
  
    private void EmitValueChanged(double? newValue)
    {
      this.EmitValueChanged(_EvaluationContext.EvaluationContextKey, this.Symbol.Value, newValue);
    }
}
}
