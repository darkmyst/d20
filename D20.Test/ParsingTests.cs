﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using D20.ExpressionParsers.Parsers;
using D20.ExpressionParsers.Parsers.ExpressionNodes;

namespace D20.Test
{
  [TestClass]
  public class ParsingTests
  {
    [TestMethod]
    public void ArithmeticEvaluationParserTest()
    {
      var parser = new EvaluationParser();
      var inputs = new[] { "2", "2+3", "6-4", "6-4/2", "6+-3", "6*-(2+3)", "2^3", "2^-3", "(2)+(3)", "4^3^2", "(2+3)^(6^-2)" };
      var expected = new[] { 2, 2 + 3, 6 - 4, 6 - 4 / 2, 6 + -3, 6 * -(2 + 3), Math.Pow(2, 3), Math.Pow(2, -3), (2) + (3), Math.Pow(4, Math.Pow(3, 2)), Math.Pow((2 + 3), Math.Pow(6, -2)) };

      for (int i = 0; i < inputs.Length; i++)
      {
        double output;
        Assert.IsTrue(parser.TryParse(out output, inputs[i]));
        Assert.AreEqual(expected[i], output);
      }
    }

    [TestMethod]
    public void ArithmeticTreeParserTest()
    {
      var parser = new TreeParser();
      var inputs = new[] { "2", "2+3", "6 - 4", "6-4/2", "6+-3", "6*-(2+3)", "2^3", "2^-3", "(2)+(3)", "4^3^2", "(2+3)^(6^-2)", "2^(1+3)" };
      var expectedToString = new[] { "2", "2 + 3", "6 - 4", "6 - 4 / 2", "6 + -3", "6 * -(2 + 3)", "2 ^ 3", "2 ^ -3", "2 + 3", "4 ^ 3 ^ 2", "(2 + 3) ^ 6 ^ -2", "2 ^ (1 + 3)" };
      var expectedResult = new[] { 2, 2 + 3, 6 - 4, 6 - 4 / 2, 6 + -3, 6 * -(2 + 3), Math.Pow(2, 3), Math.Pow(2, -3), 2 + 3, Math.Pow(4, Math.Pow(3, 2)), Math.Pow((2 + 3), Math.Pow(6, -2)), Math.Pow(2, 1 + 3) };

      double result;
      for (int i = 0; i < inputs.Length; i++)
      {
        var tree = parser.Parse(inputs[i]);
        Assert.AreEqual(expectedToString[i], tree.ToString());
        Assert.IsTrue(tree.TryEvaluate(out result));
        Assert.AreEqual(expectedResult[i], result);
      }
    }

    [TestMethod]
    public void ArithmeticTreeParserWithSymbolsTest()
    {
      var symbols = new Dictionary<string, double>()
      {
        { "a", 1 },
        //{ "b", 2 },
        { "c", 3 },
        { "INT", 4 },
        { "_other", 5 },
      };

      var parser = new TreeParser();
      var inputs = new[] { "a", "a+b", "INT / 2", "_other - +_other", "_other + -_other", "a^(b+c)", "(ability - 10) / 2" };
      var expectedToString = new[] { "a", "a + b", "INT / 2", "_other - _other", "_other + -_other", "a ^ (b + c)", "(ability - 10) / 2" };
      var expectedToStringWithContext = new[] 
        {
          string.Format("{0}", symbols["a"]), 
          string.Format("{0} + b", symbols["a"]),// symbols["b"]), 
          string.Format("{0} / 2", symbols["INT"]), 
          string.Format("{0} - {0}", symbols["_other"]), 
          string.Format("{0} + -{0}", symbols["_other"]), 
          string.Format("{0} ^ (b + {1})", symbols["a"], symbols["c"]),
          "(ability - 10) / 2"
        };

      EvaluationContext context = new EvaluationContext(symbolValues: symbols);
      for (int i = 0; i < inputs.Length; i++)
      {
        var tree = parser.Parse(inputs[i]);
        Assert.AreEqual(expectedToString[i], tree.ToString());
        Assert.AreEqual(expectedToStringWithContext[i], tree.ToString(context));
      }
    }

    [TestMethod]
    public void ArithmeticTreeParserBasicFunctionTest()
    {
      var inputs = new[] { "Pi()", "Square(3)", "Pow(2,4)", "Square(2+2)" };
      var expectedToString = new[] { "Pi()", "Square(3)", "Pow(2, 4)", "Square(2 + 2)" };
      var expectedResult = new[] { Math.PI, 3 * 3, Math.Pow(2, 4), 4 * 4 };

      var parser = new TreeParser();

      EvaluationContext context = new EvaluationContext();
      context.RegisterFunction("Pi", () => Math.PI);
      context.RegisterFunction("Square", x => x * x);
      context.RegisterFunction("Pow", (x, y) => Math.Pow(x, y));

      double result;
      for (int i = 0; i < inputs.Length; i++)
      {
        var tree = parser.Parse(inputs[i]);
        Assert.AreEqual(expectedToString[i], tree.ToString());
        Assert.IsTrue(tree.TryEvaluate(out result, context));
        Assert.AreEqual(expectedResult[i], result);
      }
    }

    [TestMethod]
    public void ArithmeticTreeParserFunctionsWithSymbols()
    {
      var parser = new TreeParser();
      var context = new EvaluationContext();
      context.RegisterFunction("RoundDown", x => x >= 0 ? Math.Truncate(x) : Math.Round(x, MidpointRounding.AwayFromZero));
      var tree = parser.Parse("RoundDown((ability - 10)/2)");
      double result;

      Assert.AreEqual("RoundDown((ability - 10) / 2)", tree.ToString());

      context.SetValueForSymbol("ability", 7);
      Assert.AreEqual("RoundDown((7 - 10) / 2)", tree.ToString(context));
      Assert.IsTrue(tree.TryEvaluate(out result, context));
      Assert.AreEqual(-2, result);

      context.SetValueForSymbol("ability", 8);
      Assert.AreEqual("RoundDown((8 - 10) / 2)", tree.ToString(context));
      Assert.IsTrue(tree.TryEvaluate(out result, context));
      Assert.AreEqual(-1, result);

      context.SetValueForSymbol("ability", 9);
      Assert.AreEqual("RoundDown((9 - 10) / 2)", tree.ToString(context));
      Assert.IsTrue(tree.TryEvaluate(out result, context));
      Assert.AreEqual(-1, result);

      context.SetValueForSymbol("ability", 10);
      Assert.AreEqual("RoundDown((10 - 10) / 2)", tree.ToString(context));
      Assert.IsTrue(tree.TryEvaluate(out result, context));
      Assert.AreEqual(0, result);

      context.SetValueForSymbol("ability", 11);
      Assert.AreEqual("RoundDown((11 - 10) / 2)", tree.ToString(context));
      Assert.IsTrue(tree.TryEvaluate(out result, context));
      Assert.AreEqual(0, result);

      context.SetValueForSymbol("ability", 12);
      Assert.AreEqual("RoundDown((12 - 10) / 2)", tree.ToString(context));
      Assert.IsTrue(tree.TryEvaluate(out result, context));
      Assert.AreEqual(1, result);
    }
    

    [TestMethod]
    public void ArithmeticTreeParserWithSimpleEqualityTest()
    {

      var parser = new TreeParser();
      var inputs = new[] { "X=2", "Y=2+3", "Z=X*(6-4)" };
      var expected = new[] { "X = 2", "Y = 2 + 3", "Z = X * (6 - 4)" };

      for (int i = 0; i < inputs.Length; i++)
      {
        var rootTreeNode = parser.Parse(inputs[i]);
        Assert.IsNotNull(rootTreeNode);
        Assert.AreEqual(expected[i], rootTreeNode.ToString());
      }
    }

    [TestMethod]
    public void ArithmeticTreeParserWithComplexEqualityTest()
    {

      var parser = new TreeParser();
      var inputs = new[] { "X -6=2", "Y+Z=2+3", "Z/3=X*(6-4)" };
      var expected = new[] { "X - 6 = 2", "Y + Z = 2 + 3", "Z / 3 = X * (6 - 4)" };

      for (int i = 0; i < inputs.Length; i++)
      {
        var rootTreeNode = parser.Parse(inputs[i]);
        Assert.IsNotNull(rootTreeNode);
        Assert.AreEqual(expected[i], rootTreeNode.ToString());
      }
    }

    [TestMethod]
    public void ArithmeticTreeParserWithMulipleEqualityFailsTest()
    {

      var parser = new TreeParser();
      var inputs = new[] { "X -6=2=3+Z", "Y+Z=2+3=A", "Z/3=X*(6-4)=Y" };
      int exceptionsCaught = 0;

      for (int i = 0; i < inputs.Length; i++)
      {
        try
        {
          var rootTreeNode = parser.Parse(inputs[i]);
        }
        catch (TreeParseException ex)
        {
          exceptionsCaught++;
        }
      }
      Assert.AreEqual(3, exceptionsCaught);
    }

    [TestMethod]
    public void ArithmeticExtendedEvaluationContextSymbolsTest()
    {
      var parser = new TreeParser();
      var context1 = new EvaluationContext();    
      var context2 = new EvaluationContext(expandedContext: context1);
      
      context1.SetValueForSymbol("X", 2);
      context2.SetValueForSymbol("Y", 3);

      var input = new [] { "X", "Y", "X+Y", "X*Y" };
      var expected = new[] { 2, 3, 5, 6};

      for(int i= 0, n = input.Length; i < n; i++)
      {
        var tree = parser.Parse(input[i]);
        double result;
        Assert.IsTrue(tree.TryEvaluate(out result, context2));
        Assert.AreEqual(expected[i], result);
      }
    }

    [TestMethod]
    public void ArithmeticExtendedEvaluationContextFunctionsTest()
    {
      var parser = new TreeParser();
      var context1 = new EvaluationContext();    
      var context2 = new EvaluationContext(expandedContext: context1);
      
      context1.RegisterFunction("Square", x => x * x);
      context2.RegisterFunction("Cube", x => x * x * x);

      var input = new [] { "Square(2)", "Cube(2)" };
      var expected = new[] { 4, 8 };

      for(int i= 0, n = input.Length; i < n; i++)
      {
        var tree = parser.Parse(input[i]);
        double result;
        Assert.IsTrue(tree.TryEvaluate(out result, context2));
        Assert.AreEqual(expected[i], result);
      }
    }
  }
}
