﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using D20.ExpressionParsers.Tokenizers;

namespace D20.Test
{
  [TestClass]
  public class D20Tests
  {
    [TestMethod]
    public void D20ValueScopeRegisterUnregisterSimpleTest()
    {
      var input = "X = 2";
      var d20ValueScope = new D20ValueScope();
      ID20ComplexValue d20Value = d20ValueScope.CreateFromExpression(input);

      Assert.IsNotNull(d20Value);
      Assert.IsNotNull(d20Value.Symbol);
      Assert.AreEqual(TokenType.Symbol, d20Value.Symbol.Type);
      Assert.AreEqual("X", d20Value.Symbol.Value);
      Assert.AreEqual("2", d20Value.CoreExpression);
      Assert.AreEqual(2, d20Value.Value);
    }

    [TestMethod]
    public void D20ValueScopeRegisterSlightlyLessSimpleTest()
    {
      var input = "X = 2+3";
      var d20ValueScope = new D20ValueScope();
      ID20ComplexValue d20Value = d20ValueScope.CreateFromExpression(input);

      Assert.IsNotNull(d20Value);
      Assert.IsNotNull(d20Value.Symbol);
      Assert.AreEqual(TokenType.Symbol, d20Value.Symbol.Type);
      Assert.AreEqual("X", d20Value.Symbol.Value);
      Assert.AreEqual("2 + 3", d20Value.CoreExpression);
      Assert.AreEqual(5, d20Value.Value);
    }

    [TestMethod]
    public void D20ValueScopeGetBySymbolTest()
    {
      var valueInput = "X = 2+3";
      var d20ValueScope = new D20ValueScope();
      ID20Value registeredD20Value = d20ValueScope.CreateFromExpression(valueInput);
      ID20Value fetchedD20Value = d20ValueScope.GetValue("X");

      Assert.AreSame(registeredD20Value, fetchedD20Value);
    }

    [TestMethod]
    public void D20ValueScopeUnregisterTest()
    {
      var valueInput = "X = 2+3";
      var d20ValueScope = new D20ValueScope();
      ID20Value registeredD20Value = d20ValueScope.CreateFromExpression(valueInput);
      d20ValueScope.Unregister("X");
      ID20Value fetchedD20Value = d20ValueScope.GetValue("X");

      Assert.IsNull(fetchedD20Value);
    }

    [TestMethod]
    public void D20ValueScopeRegisterWithUnknownSymbolInExpressionTest()
    {
      var valueInput = "X = 2+a";
      var d20ValueScope = new D20ValueScope();
      ID20ComplexValue registeredD20Value = d20ValueScope.CreateFromExpression(valueInput);

      Assert.IsNull(registeredD20Value.Value);
    }

    [TestMethod]
    public void D20ValueScopeTwoRelatedRegisteredSymbolsTest()
    {
      var d20ValueScope = new D20ValueScope();
      ID20SimpleValue xValue = d20ValueScope.Create("X", 2);
      ID20ComplexValue yValue = d20ValueScope.CreateFromExpression("Y = X * 3");

      Assert.AreEqual(2, d20ValueScope.GetValue("X").Value);
      Assert.AreEqual(6, d20ValueScope.GetValue("Y").Value);

      xValue.Value = 3;
      Assert.AreEqual(3, d20ValueScope.GetValue("X").Value);
      Assert.AreEqual(9, d20ValueScope.GetValue("Y").Value);
    }

    [TestMethod]
    public void D20ValueScopeThreeRelatedRegisteredSymbolsTest()
    {
      var d20ValueScope = new D20ValueScope();
      ID20SimpleValue xValue = d20ValueScope.Create("X", 1);
      ID20ComplexValue yValue = d20ValueScope.CreateFromExpression("Y = X * 3");
      ID20ComplexValue zValue = d20ValueScope.CreateFromExpression("Z = X ^ Y");

      Assert.AreEqual(1, d20ValueScope.GetValue("X").Value);
      Assert.AreEqual(3, d20ValueScope.GetValue("Y").Value);
      Assert.AreEqual(1, d20ValueScope.GetValue("Z").Value);

      xValue.Value = 2;
      Assert.AreEqual(2, d20ValueScope.GetValue("X").Value);
      Assert.AreEqual(6, d20ValueScope.GetValue("Y").Value);
      Assert.AreEqual(64, d20ValueScope.GetValue("Z").Value);

      xValue.Value = 3;
      Assert.AreEqual(3, d20ValueScope.GetValue("X").Value);
      Assert.AreEqual(9, d20ValueScope.GetValue("Y").Value);
      Assert.AreEqual(19683, d20ValueScope.GetValue("Z").Value);
    }

    [TestMethod]
    public void D20ValueScopesDontShareScope()
    {
      var container1 = new D20ValueScope();
      var container2 = new D20ValueScope();
      var x1 = container1.Create("X", 1);
      var x2 = container2.Create("X", 2);

      Assert.AreNotSame(x1, x2);
      Assert.AreEqual(1, x1.Value);
      Assert.AreEqual(2, x2.Value);
    }

    [TestMethod]
    public void D20ValueScopeRegisterRelatedSymbolsOutOfOrderTest()
    {
      var container = new D20ValueScope();
      var intmod = container.CreateFromExpression("Int.Mod = Int * 2");
      var intScore = container.Create("Int", 5);

      Assert.AreEqual(5, container.GetValue("Int").Value);
      Assert.AreEqual(10, container.GetValue("Int.Mod").Value);

      intScore.Value = 15;
      Assert.AreEqual(15, container.GetValue("Int").Value);
      Assert.AreEqual(30, container.GetValue("Int.Mod").Value);
    }

    [TestMethod]
    public void D20ValueScopeRegisterNestedFunctionTest()
    {
      var container1 = new D20ValueScope();
      container1.RegisterFunction("RoundDown", x => x >= 0 ? Math.Truncate(x) : Math.Round(x, MidpointRounding.AwayFromZero));
      container1.RegisterFunction("CalcAbilityMod(ability) = RoundDown((ability - 10)/2)");
      var intmod = container1.CreateFromExpression("Int.Mod = CalcAbilityMod(Int)");
      var intScore = container1.Create("Int", 13);

      Assert.AreEqual(13, container1.GetValue("Int").Value);
      Assert.AreEqual(1, container1.GetValue("Int.Mod").Value);

      intScore.Value = 14;
      Assert.AreEqual(14, container1.GetValue("Int").Value);
      Assert.AreEqual(2, container1.GetValue("Int.Mod").Value);
    }

    [TestMethod]
    public void D20FullOnCharacterSheetTest()
    {
      var characterSheet = new D20ValueScope();

      characterSheet.RegisterFunction("RoundDown", x => x >= 0 ? Math.Truncate(x) : Math.Round(x, MidpointRounding.AwayFromZero));
      characterSheet.RegisterFunction("CalcAbilityMod(ability) = RoundDown((ability - 10)/2)");

      var strScore = characterSheet.Create("STR.Score", 9);
      var dexScore = characterSheet.Create("DEX.Score", 10);
      var conScore = characterSheet.Create("CON.Score", 11);
      var intScore = characterSheet.Create("INT.Score", 12);
      var wisScore = characterSheet.Create("WIS.Score", 13);
      var chaScore = characterSheet.Create("CHA.Score", 14);
      var baseAttackBonus = characterSheet.Create("BAB", 3);
      var sizeMod = characterSheet.Create("SizeMod", 1);
      var currentArmorBonus = characterSheet.Create("CurrentArmorBonus", 3);
      var currentShieldBonus = characterSheet.Create("CurrentShieldBonus", 0);

      var strMod = characterSheet.CreateFromExpression("STR = CalcAbilityMod(STR.Score)");
      var dexMod = characterSheet.CreateFromExpression("DEX = CalcAbilityMod(DEX.Score)");
      var conMod = characterSheet.CreateFromExpression("CON = CalcAbilityMod(CON.Score)");
      var intMod = characterSheet.CreateFromExpression("INT = CalcAbilityMod(INT.Score)");
      var wisMod = characterSheet.CreateFromExpression("WIS = CalcAbilityMod(WIS.Score)");
      var chaMod = characterSheet.CreateFromExpression("CHA = CalcAbilityMod(CHA.Score)");
      var cmb = characterSheet.CreateFromExpression("CMB = 10 + BAB + STR + DEX + SizeMod");
      var ac = characterSheet.CreateFromExpression("AC = 10 + CurrentArmorBonus + CurrentShieldBonus + DEX + SizeMod");

      Assert.AreEqual(9, strScore.Value);
      Assert.AreEqual(10, dexScore.Value);
      Assert.AreEqual(11, conScore.Value);
      Assert.AreEqual(12, intScore.Value);
      Assert.AreEqual(13, wisScore.Value);
      Assert.AreEqual(14, chaScore.Value);
      Assert.AreEqual(1, sizeMod.Value);
      Assert.AreEqual(3, currentArmorBonus.Value);
      Assert.AreEqual(0, currentShieldBonus.Value);

      Assert.AreEqual(-1, strMod.Value);
      Assert.AreEqual(0, dexMod.Value);
      Assert.AreEqual(0, conMod.Value);
      Assert.AreEqual(1, intMod.Value);
      Assert.AreEqual(1, wisMod.Value);
      Assert.AreEqual(2, chaMod.Value);
      Assert.AreEqual(13, cmb.Value);
      Assert.AreEqual(14, ac.Value);

      dexScore.Value = 18;
      Assert.AreEqual(9, strScore.Value);
      Assert.AreEqual(18, dexScore.Value);
      Assert.AreEqual(11, conScore.Value);
      Assert.AreEqual(12, intScore.Value);
      Assert.AreEqual(13, wisScore.Value);
      Assert.AreEqual(14, chaScore.Value);
      Assert.AreEqual(1, sizeMod.Value);

      Assert.AreEqual(-1, strMod.Value);
      Assert.AreEqual(4, dexMod.Value);
      Assert.AreEqual(0, conMod.Value);
      Assert.AreEqual(1, intMod.Value);
      Assert.AreEqual(1, wisMod.Value);
      Assert.AreEqual(2, chaMod.Value);
      Assert.AreEqual(17, cmb.Value);
      Assert.AreEqual(18, ac.Value);
    }

    [TestMethod]
    public void D20ValuesSupportSimpleModifiers()
    {
      var d20ValueScope = new D20ValueScope();
      ID20ComplexValue d20Value = d20ValueScope.CreateFromExpression("X = 3");
      ID20SimpleValue d20ValueMod = d20ValueScope.Create("Modifiers.Simple", 2);
      Assert.AreEqual(3, d20Value.Value);

      d20Value.AddModifier(d20ValueMod);
      Assert.AreEqual(5, d20Value.Value);

      d20ValueMod.Value = -1;
      Assert.AreEqual(2, d20Value.Value);

      d20Value.RemoveModifier(d20ValueMod.Symbol.Value);
      Assert.AreEqual(3, d20Value.Value);
    }

    [TestMethod]
    public void D20ValueScopeLoadAndReadJsonSupport()
    {
      string dataKey = "PointBuyCosts";
      string dataJson = @"{ ""7"": -4, ""8"": -2, ""9"": -1, ""10"": 0, ""11"": 1, ""12"": 2, ""13"": 3, ""14"": 5, ""15"": 7, ""16"": 10, ""17"": 13, ""18"": 17 }";
      var d20ValueScope = new D20ValueScope();
      d20ValueScope.LoadData(name: dataKey, json: dataJson);
      var result = d20ValueScope.ReadData(path: string.Format(@"PointBuyCosts[""{0}""]", 7));

      Assert.IsTrue(d20ValueScope.HasDataKey(key: dataKey));
      Assert.IsNotNull(result);
      Assert.AreEqual((double)-4, (double)result);

      result = d20ValueScope.ReadData(path: string.Format(@"PointBuyCosts[""{0}""]", 14));

      Assert.IsTrue(d20ValueScope.HasDataKey(key: dataKey));
      Assert.IsNotNull(result);
      Assert.AreEqual((double)5, (double)result);
    }

    [TestMethod]
    public void D20ValueScopeLoadAndReadJsonViaFunctionSupport()
    {
      string dataKey = "PointBuyCosts";
      string dataJson = @"{ ""7"": -4, ""8"": -2, ""9"": -1, ""10"": 0, ""11"": 1, ""12"": 2, ""13"": 3, ""14"": 5, ""15"": 7, ""16"": 10, ""17"": 13, ""18"": 17 }";
      var d20ValueScope = new D20ValueScope();
      d20ValueScope.LoadData(name: dataKey, json: dataJson);
      d20ValueScope.RegisterFunction("GetPointBuyCost", x => (double)d20ValueScope.ReadData(string.Format(@"PointBuyCosts[""{0}""]", x)));
      var yVal = d20ValueScope.Create("Y", 10);
      var xVal = d20ValueScope.CreateFromExpression("X = GetPointBuyCost(Y)");

      Assert.AreEqual(0, xVal.Value);

      yVal.Value = 14;
      Assert.AreEqual(5, xVal.Value);
    }
  }
}
