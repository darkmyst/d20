﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Json.Tokenizers;
using System.Collections.Generic;
using Json.Parsers;
using Json.Parsers.TreeNodes;

namespace Json.Test
{
  [TestClass]
  public class JsonParsingTests
  {
    #region Samples
    private static readonly string Sample1 = @"{
    ""widget"": {
        ""debug"": ""on"",
        ""window"": {
            ""title"": ""Sample Konfabulator Widget"",
            ""name"": ""main_window"",
            ""width"": 500,
            ""height"": 500
        },
        ""image"": {
            ""src"": ""Images/Sun.png"",
            ""name"": ""sun1"",
            ""hOffset"": 250,
            ""vOffset"": 250,
            ""alignment"": ""center""
        },
        ""text"": {
            ""data"": ""Click Here"",
            ""size"": 36,
            ""style"": ""bold"",
            ""name"": ""text1"",
            ""hOffset"": 250,
            ""vOffset"": 100,
            ""alignment"": ""center"",
            ""onMouseUp"": ""sun1.opacity = (sun1.opacity / 100) * 90;"",
            ""metadata"": [
              ""item0"", ""item1"", ""item2"", { ""item3"" : true }, 4
            ]
        }
    }
}";

    private static readonly string Sample2 = @"{
    ""web-app"": {
        ""servlet"": [
            {
                ""servlet-name"": ""cofaxCDS"",
                ""servlet-class"": ""org.cofax.cds.CDSServlet"",
                ""init-param"": {
                    ""configGlossary:installationAt"": ""Philadelphia, PA"",
                    ""configGlossary:adminEmail"": ""ksm@pobox.com"",
                    ""configGlossary:poweredBy"": ""Cofax"",
                    ""configGlossary:poweredByIcon"": ""/images/cofax.gif"",
                    ""configGlossary:staticPath"": ""/content/static"",
                    ""templateProcessorClass"": ""org.cofax.WysiwygTemplate"",
                    ""templateLoaderClass"": ""org.cofax.FilesTemplateLoader"",
                    ""templatePath"": ""templates"",
                    ""templateOverridePath"": """",
                    ""defaultListTemplate"": ""listTemplate.htm"",
                    ""defaultFileTemplate"": ""articleTemplate.htm"",
                    ""useJSP"": false,
                    ""jspListTemplate"": ""listTemplate.jsp"",
                    ""jspFileTemplate"": ""articleTemplate.jsp"",
                    ""cachePackageTagsTrack"": 200,
                    ""cachePackageTagsStore"": 200,
                    ""cachePackageTagsRefresh"": 60,
                    ""cacheTemplatesTrack"": 100,
                    ""cacheTemplatesStore"": 50,
                    ""cacheTemplatesRefresh"": 15,
                    ""cachePagesTrack"": 200,
                    ""cachePagesStore"": 100,
                    ""cachePagesRefresh"": 10,
                    ""cachePagesDirtyRead"": 10,
                    ""searchEngineListTemplate"": ""forSearchEnginesList.htm"",
                    ""searchEngineFileTemplate"": ""forSearchEngines.htm"",
                    ""searchEngineRobotsDb"": ""WEB-INF/robots.db"",
                    ""useDataStore"": true,
                    ""dataStoreClass"": ""org.cofax.SqlDataStore"",
                    ""redirectionClass"": ""org.cofax.SqlRedirection"",
                    ""dataStoreName"": ""cofax"",
                    ""dataStoreDriver"": ""com.microsoft.jdbc.sqlserver.SQLServerDriver"",
                    ""dataStoreUrl"": ""jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon"",
                    ""dataStoreUser"": ""sa"",
                    ""dataStorePassword"": ""dataStoreTestQuery"",
                    ""dataStoreTestQuery"": ""SET NOCOUNT ON;select test='test';"",
                    ""dataStoreLogFile"": ""/usr/local/tomcat/logs/datastore.log"",
                    ""dataStoreInitConns"": 10,
                    ""dataStoreMaxConns"": 100,
                    ""dataStoreConnUsageLimit"": 100,
                    ""dataStoreLogLevel"": ""debug"",
                    ""maxUrlLength"": 500
                }
            },
            {
                ""servlet-name"": ""cofaxEmail"",
                ""servlet-class"": ""org.cofax.cds.EmailServlet"",
                ""init-param"": {
                    ""mailHost"": ""mail1"",
                    ""mailHostOverride"": ""mail2""
                }
            },
            {
                ""servlet-name"": ""cofaxAdmin"",
                ""servlet-class"": ""org.cofax.cds.AdminServlet""
            },
            {
                ""servlet-name"": ""fileServlet"",
                ""servlet-class"": ""org.cofax.cds.FileServlet""
            },
            {
                ""servlet-name"": ""cofaxTools"",
                ""servlet-class"": ""org.cofax.cms.CofaxToolsServlet"",
                ""init-param"": {
                    ""templatePath"": ""toolstemplates/"",
                    ""log"": 1,
                    ""logLocation"": ""/usr/local/tomcat/logs/CofaxTools.log"",
                    ""logMaxSize"": """",
                    ""dataLog"": 1,
                    ""dataLogLocation"": ""/usr/local/tomcat/logs/dataLog.log"",
                    ""dataLogMaxSize"": """",
                    ""removePageCache"": ""/content/admin/remove?cache=pages&id="",
                    ""removeTemplateCache"": ""/content/admin/remove?cache=templates&id="",
                    ""fileTransferFolder"": ""/usr/local/tomcat/webapps/content/fileTransferFolder"",
                    ""lookInContext"": 1,
                    ""adminGroupID"": 4,
                    ""betaServer"": true
                }
            }
        ],
        ""servlet-mapping"": {
            ""cofaxCDS"": ""/"",
            ""cofaxEmail"": ""/cofaxutil/aemail/*"",
            ""cofaxAdmin"": ""/admin/*"",
            ""fileServlet"": ""/static/*"",
            ""cofaxTools"": ""/tools/*""
        },
        ""taglib"": {
            ""taglib-uri"": ""cofax.tld"",
            ""taglib-location"": ""/WEB-INF/tlds/cofax.tld""
        }
    }
}";
    #endregion

    [TestMethod]
    public void JsonTokenizerReadsEmptyStringTest()
    {
      string input = @""""""; // ""
      string expected = "";
      _ExecuteGreenStringTokenizerTest(JsonTokenType.String, input, expected);
    }

    [TestMethod]
    public void JsonTokenizerReadsSimpleStringTest()
    {
      string input = @"""abc 123"""; // "abc 123"
      string expected = "abc 123";
      _ExecuteGreenStringTokenizerTest(JsonTokenType.String, input, expected);
    }

    [TestMethod]
    public void JsonTokenizerReadsStringWithEscapedCharsTest()
    {
      string input = "\"\\\"foo\\\" is not \\\"bar\\\". specials: \\b\\r\\n\\f\\t\\\\\\/\"";
      string expected = "\\\"foo\\\" is not \\\"bar\\\". specials: \\b\\r\\n\\f\\t\\\\\\/";

      _ExecuteGreenStringTokenizerTest(JsonTokenType.String, input, expected);
    }

    [TestMethod]
    public void JsonTokenizerReadsTrueAndFalseTest()
    {
      var inputs = new string[] { "true", "false", "null" };
      var expected = new bool?[] { true, false, null };

      foreach (var data in inputs.Zip(expected, (x, y) => new KeyValuePair<string, bool?>(x, y)))
        _ExecuteGreenStringTokenizerTest(JsonTokenType.Bool, data.Key, data.Value);
    }

    [TestMethod]
    public void JsonTokenizerReadsNumbersTest()
    {
      var inputs = new string[] { "0", "1", "-2", "3.4", "-5.67", "0.8", "-0.910" };
      var expected = new double[] { 0, 1, -2, 3.4, -5.67, 0.8, -0.910 };

      foreach (var data in inputs.Zip(expected, (x, y) => new KeyValuePair<string, double>(x, y)))
        _ExecuteGreenStringTokenizerTest(JsonTokenType.Number, data.Key, data.Value);
    }

    [TestMethod]
    public void JsonTokenizerReadsDelimitersTest()
    {
      var inputs = new string[] { "[", "]", "{", "}", ":", "," };
      var expectedTypes = new JsonTokenType[] { JsonTokenType.LBracket, JsonTokenType.RBracket, JsonTokenType.LCurly, JsonTokenType.RCurly, JsonTokenType.Colon, JsonTokenType.Comma };

      foreach (var data in inputs.Zip(expectedTypes, (x, y) => new { Input = x, ExpectedType = y }))
        _ExecuteGreenStringTokenizerTest(data.ExpectedType, data.Input, data.Input);
    }

    [TestMethod]
    public void JsonTokenizerReadsMultipleTokensTest()
    {
      var input = @"0 1 ""Hello!"" true null 2.3";
      var expectedTypes = new JsonTokenType[] { JsonTokenType.Number, JsonTokenType.Number, JsonTokenType.String, JsonTokenType.Bool, JsonTokenType.Bool, JsonTokenType.Number, JsonTokenType.End };
      var expectedValues = new object[] { (double)0, (double)1, (string)"Hello!", (bool?)true, (bool?)null, (double)2.3, (object)null };

      var expected = expectedTypes.Zip(expectedValues, (_type, _value) =>
        new { Type = _type, Value = _value }).ToArray();

      var tokenizer = new JsonTokenizer(input);
      IJsonToken token;
      int i = 0;
      while (tokenizer.TryReadToken(out token))
      {
        Assert.AreEqual(expected[i].Type, token.Type);
        Assert.AreEqual(expected[i].Value, token.Value);
        i++;
      }
      Assert.AreEqual(JsonTokenType.End, token.Type);
      Assert.AreEqual(null, token.Value);
    }


    [TestMethod]
    public void JsonTokenizerReadsBigSampleTest()
    {
      foreach (var input in new[] { Sample1, Sample2 })
      {
        var tokenizer = new JsonTokenizer(input);
        IJsonToken token;
        while (tokenizer.TryReadToken(out token))
        {
          // This is a lot of tokens. The only purpose of this test
          // is to ensure that the parser doesn't encounter unknown
          // tokens. It's not a great test, mind you, but it's something
          Assert.AreNotEqual(JsonTokenType.End, token.Type);
          Assert.IsNotNull(token.Value);
        }

        Assert.AreEqual(JsonTokenType.End, token.Type);
        Assert.AreEqual(null, token.Value);
      }
    }

    [TestMethod]
    public void JsonParserParsesBigSampleTest()
    {
      foreach (var input in new[] { Sample1, Sample2 })
      {
        var parser = new JsonTreeParser();
        var output = parser.Parse(input);
        Assert.IsNotNull(output);
        Assert.AreEqual(TreeItemType.Object, output.Type);
      }
    }

    [TestMethod]
    public void JsonTreeItemReadWithObjectDotNotationTest()
    {
      var input = Sample1;
      var parser = new JsonTreeParser();
      var output = parser.Parse(input);
      Assert.IsNotNull(output);

      var value = output.Read("widget.debug");
      Assert.IsNotNull(value);
      Assert.AreEqual("on", value);

      value = output.Read("widget.window");
      Assert.IsNotNull(value);
      Assert.IsInstanceOfType(value, typeof(IJsonTreeItem));
      Assert.AreEqual(TreeItemType.Object, ((IJsonTreeItem)value).Type);
      
      value = output.Read("widget.text.vOffset");
      Assert.IsNotNull(value);
      Assert.AreEqual(100.0, value);
      
      value = output.Read("widget.text.metadata[3]");
      Assert.IsNotNull(value);
      Assert.IsInstanceOfType(value, typeof(IJsonTreeItem));
      Assert.AreEqual(TreeItemType.Object, ((IJsonTreeItem)value).Type);

      value = output.Read("widget.text.metadata");
      Assert.IsNotNull(value);
      Assert.IsInstanceOfType(value, typeof(IJsonTreeItem));
      Assert.AreEqual(TreeItemType.Array, ((IJsonTreeItem)value).Type);

      var metadataArray = (IJsonTreeItem)value;
      value = metadataArray.Read("this[3]");
      Assert.IsNotNull(value);
      Assert.IsInstanceOfType(value, typeof(IJsonTreeItem));
      Assert.AreEqual(TreeItemType.Object, ((IJsonTreeItem)value).Type);
      
      var subMetadataObject = (IJsonTreeItem)value;
      value = subMetadataObject.Read("item3");
      Assert.IsNotNull(value);
      Assert.AreEqual(true, value);

    }    
    
    [TestMethod]
    public void JsonTreeItemReadWithObjectIndexNotationTest()
    {
      var input = Sample1;
      var parser = new JsonTreeParser();
      var output = parser.Parse(input);
      Assert.IsNotNull(output);

      var value = output.Read(@"this[""widget""][""debug""]");
      Assert.IsNotNull(value);
      Assert.AreEqual("on", value);

      value = output.Read(@"this[""widget""][""window""]");
      Assert.IsNotNull(value);
      Assert.IsInstanceOfType(value, typeof(IJsonTreeItem));
      Assert.AreEqual(TreeItemType.Object, ((IJsonTreeItem)value).Type);
      
      value = output.Read(@"this[""widget""][""text""][""vOffset""]");
      Assert.IsNotNull(value);
      Assert.AreEqual(100.0, value);
      
      value = output.Read(@"this[""widget""][""text""][""metadata""][3]");
      Assert.IsNotNull(value);
      Assert.IsInstanceOfType(value, typeof(IJsonTreeItem));
      Assert.AreEqual(TreeItemType.Object, ((IJsonTreeItem)value).Type);

      value = output.Read(@"this[""widget""][""text""][""metadata""]");
      Assert.IsNotNull(value);
      Assert.IsInstanceOfType(value, typeof(IJsonTreeItem));
      Assert.AreEqual(TreeItemType.Array, ((IJsonTreeItem)value).Type);

      var metadataArray = (IJsonTreeItem)value;
      value = metadataArray.Read("this[3]");
      Assert.IsNotNull(value);
      Assert.IsInstanceOfType(value, typeof(IJsonTreeItem));
      Assert.AreEqual(TreeItemType.Object, ((IJsonTreeItem)value).Type);
      
      var subMetadataObject = (IJsonTreeItem)value;
      value = subMetadataObject.Read(@"this[""item3""]");
      Assert.IsNotNull(value);
      Assert.AreEqual(true, value);

    }

    [TestMethod]
    public void JsonTreeManualCreationTest()
    {
      var parser = new JsonTreeParser();
      var expected = parser.Parse(@"[ ""Hello"", 1.23, true, false, null, [  ], { ""Key"": 6.66 } ]").ToString();

      JsonArrayTreeNode arrayNode = new JsonArrayTreeNode();
      arrayNode.Children.Add(new JsonValueTreeNode<string>("Hello"));
      arrayNode.Children.Add(new JsonValueTreeNode<double>(1.23));
      arrayNode.Children.Add(new JsonValueTreeNode<bool?>(true));
      arrayNode.Children.Add(new JsonValueTreeNode<bool?>(false));
      arrayNode.Children.Add(new JsonValueTreeNode<bool?>(null));

      JsonArrayTreeNode childArrayNode = new JsonArrayTreeNode();
      arrayNode.Children.Add(new JsonValueTreeNode<JsonArrayTreeNode>(childArrayNode));

      JsonObjectTreeNode childObjectNode = new JsonObjectTreeNode();
      childObjectNode.Children.Add("Key", new JsonValueTreeNode<double>(6.66));
      arrayNode.Children.Add(new JsonValueTreeNode<JsonObjectTreeNode>(childObjectNode));

      Assert.IsNotNull(arrayNode);

      Assert.AreEqual(expected, arrayNode.ToString());
    }

    private void _ExecuteGreenStringTokenizerTest<T>(JsonTokenType expectedTokenType, string input, T expected)
    {
      var tokenizer = new JsonTokenizer(input);
      IJsonToken token;
      Assert.IsTrue(tokenizer.TryReadToken(out token));
      Assert.AreEqual(expectedTokenType, token.Type);
      Assert.AreEqual(expected, token.Value);

      Assert.IsFalse(tokenizer.TryReadToken(out token));
      Assert.AreEqual(JsonTokenType.End, token.Type);
      Assert.AreEqual(null, token.Value);
    }
  }
}
