﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace D20.App
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private MainWindowViewModel _MainWindowViewModel;
    public MainWindow()
    {
      ViewModel = new MainWindowViewModel();
      InitializeComponent();
    }

    public MainWindowViewModel ViewModel
    {
      get { return _MainWindowViewModel; }
      set { _MainWindowViewModel = value; }
    }
  }

  public class MainWindowViewModel : ReactiveObject
  {
    public D20ValueScope CharacterSheet { get; set; }

    ID20SimpleValue _TotalPoints;
    public ID20SimpleValue TotalPoints
    {
      get { return _TotalPoints; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _PointsRemaining;
    public ID20ComplexValue PointsRemaining
    {
      get { return _PointsRemaining; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ReactiveCollection<ID20Value> _SkillStuff;
    public ReactiveCollection<ID20Value> SkillStuff
    {
      get { return _SkillStuff; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    #region Ability BaseScores
    ID20SimpleValue _STRBaseScore;
    public ID20SimpleValue STRBaseScore
    {
      get { return _STRBaseScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20SimpleValue _DEXBaseScore;
    public ID20SimpleValue DEXBaseScore
    {
      get { return _DEXBaseScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20SimpleValue _CONBaseScore;
    public ID20SimpleValue CONBaseScore
    {
      get { return _CONBaseScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20SimpleValue _INTBaseScore;
    public ID20SimpleValue INTBaseScore
    {
      get { return _INTBaseScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20SimpleValue _WISBaseScore;
    public ID20SimpleValue WISBaseScore
    {
      get { return _WISBaseScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20SimpleValue _CHABaseScore;
    public ID20SimpleValue CHABaseScore
    {
      get { return _CHABaseScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }
    #endregion

    #region Ability Scores
    ID20ComplexValue _STRScore;
    public ID20ComplexValue STRScore
    {
      get { return _STRScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _DEXScore;
    public ID20ComplexValue DEXScore
    {
      get { return _DEXScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _CONScore;
    public ID20ComplexValue CONScore
    {
      get { return _CONScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _INTScore;
    public ID20ComplexValue INTScore
    {
      get { return _INTScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _WISScore;
    public ID20ComplexValue WISScore
    {
      get { return _WISScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _CHAScore;
    public ID20ComplexValue CHAScore
    {
      get { return _CHAScore; }
      set { this.RaiseAndSetIfChanged(value); }
    }
    #endregion

    #region Ability Modifiers
    ID20ComplexValue _STRModifier;
    public ID20ComplexValue STRModifier
    {
      get { return _STRModifier; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _DEXModifier;
    public ID20ComplexValue DEXModifier
    {
      get { return _DEXModifier; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _CONModifier;
    public ID20ComplexValue CONModifier
    {
      get { return _CONModifier; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _INTModifier;
    public ID20ComplexValue INTModifier
    {
      get { return _INTModifier; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _WISModifier;
    public ID20ComplexValue WISModifier
    {
      get { return _WISModifier; }
      set { this.RaiseAndSetIfChanged(value); }
    }

    ID20ComplexValue _CHAModifier;
    public ID20ComplexValue CHAModifier
    {
      get { return _CHAModifier; }
      set { this.RaiseAndSetIfChanged(value); }
    }
    #endregion


    public MainWindowViewModel()
    {
      CharacterSheet = new D20ValueScope();

      CharacterSheet.LoadData("PointBuyCosts", @"{ ""7"": -4, ""8"": -2, ""9"": -1, ""10"": 0, ""11"": 1, ""12"": 2, ""13"": 3, ""14"": 5, ""15"": 7, ""16"": 10, ""17"": 13, ""18"": 17 }");
      CharacterSheet.RegisterFunction("GetPointBuyCost", x => (double)CharacterSheet.ReadData(string.Format(@"PointBuyCosts[""{0}""]", x)));
      CharacterSheet.RegisterFunction("RoundDown", x => x >= 0 ? Math.Truncate(x) : Math.Round(x, MidpointRounding.AwayFromZero));
      CharacterSheet.RegisterFunction("CalcAbilityMod(ability) = RoundDown((ability - 10)/2)");

      // TODO: Anything outside of Point Buy Costs range is an error in point buy. Figure out a way to bake that into the string system.

      TotalPoints = CharacterSheet.Create("TotalPoints", 20);
      PointsRemaining = CharacterSheet.CreateFromExpression("PointsRemaining = TotalPoints - GetPointBuyCost(STR.Score) - GetPointBuyCost(DEX.Score) - GetPointBuyCost(CON.Score) - GetPointBuyCost(INT.Score) - GetPointBuyCost(WIS.Score) - GetPointBuyCost(CHA.Score)");

      STRBaseScore = CharacterSheet.Create("STR.Score.Base", 10);
      STRScore = CharacterSheet.CreateFromExpression("STR.Score = STR.Score.Base");
      STRModifier = CharacterSheet.CreateFromExpression("STR = CalcAbilityMod(STR.Score)");

      DEXBaseScore = CharacterSheet.Create("DEX.Score.Base", 10);
      DEXScore = CharacterSheet.CreateFromExpression("DEX.Score = DEX.Score.Base");
      DEXModifier = CharacterSheet.CreateFromExpression("DEX = CalcAbilityMod(DEX.Score)");

      CONBaseScore = CharacterSheet.Create("CON.Score.Base", 10);
      CONScore = CharacterSheet.CreateFromExpression("CON.Score = CON.Score.Base");
      CONModifier = CharacterSheet.CreateFromExpression("CON = CalcAbilityMod(CON.Score)");

      INTBaseScore = CharacterSheet.Create("INT.Score.Base", 10);
      INTScore = CharacterSheet.CreateFromExpression("INT.Score = INT.Score.Base");
      INTModifier = CharacterSheet.CreateFromExpression("INT = CalcAbilityMod(INT.Score)");

      WISBaseScore = CharacterSheet.Create("WIS.Score.Base", 10);
      WISScore = CharacterSheet.CreateFromExpression("WIS.Score = WIS.Score.Base");
      WISModifier = CharacterSheet.CreateFromExpression("WIS = CalcAbilityMod(WIS.Score)");

      CHABaseScore = CharacterSheet.Create("CHA.Score.Base", 10);
      CHAScore = CharacterSheet.CreateFromExpression("CHA.Score = CHA.Score.Base");
      CHAModifier = CharacterSheet.CreateFromExpression("CHA = CalcAbilityMod(CHA.Score)");

      CharacterSheet.RegisterFunction("GetSkillBase", (untrained, classSkill, ranks) =>
      {
        if (untrained == 0 && ranks == 0)
          throw new InvalidOperationException();
        return ranks + (classSkill > 0 ? 3 : 0);
      });

      SkillStuff = new ReactiveCollection<ID20Value>();
      SkillStuff.Add(CharacterSheet.Create("Acrobatics.Untrained", 1));
      SkillStuff.Add(CharacterSheet.Create("Acrobatics.ClassSkill", 1));
      SkillStuff.Add(CharacterSheet.Create("Acrobatics.Ranks", 1));
      SkillStuff.Add(CharacterSheet.CreateFromExpression("Acrobatics.Base = GetSkillBase(Acrobatics.Untrained, Acrobatics.ClassSkill, Acrobatics.Ranks)"));
      SkillStuff.Add(CharacterSheet.CreateFromExpression("Acrobatics.Skill = Acrobatics.Base + DEX"));
    }
  }
}
