﻿using Json.Parsers.TreeNodes;
using Json.Tokenizers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Json.Parsers
{
  public class JsonTreeParser
  {
    private JsonTokenizer _lexer;

    public JsonTreeParser()
    {
    }

    public IJsonTreeItem Parse(string input)
    {
      this._lexer = new JsonTokenizer(input);

      if (this._lexer.Peek().Type == JsonTokenType.End)
        return null;

      IJsonTreeItem rootTreeNode = ReadJson();

      if (this._lexer.Peek().Type == JsonTokenType.End)
      {
        switch (rootTreeNode.Type)
        {
          case TreeItemType.Array:
          case TreeItemType.Object:
            return rootTreeNode;
          default: throw new TreeParseException("json root node must be an array or an object");
        }
      }
      throw new TreeParseException("End Expected");
    }

    private IJsonTreeItem ReadJson()
    {
      IJsonToken token = ReadToken();

      switch(token.Type)
      {
        case JsonTokenType.LCurly:
          this._lexer.Revert();
          return ReadObject();
        case JsonTokenType.LBracket:
          this._lexer.Revert();
          return ReadArray();
        case JsonTokenType.Bool:
          return new JsonValueTreeNode<bool?>((bool?)token.Value);
        case JsonTokenType.Number:
          return new JsonValueTreeNode<double>((double)token.Value);
        case JsonTokenType.String:
          return new JsonValueTreeNode<string>((string)token.Value);
        default:
          throw new TreeParseException("Unexpected token");
      }
    }

    private IJsonTreeItem ReadObject()
    {
      IJsonToken objectStartToken = ReadToken();
      if (objectStartToken.Type != JsonTokenType.LCurly)
        throw new TreeParseException("Expected Object start token");

      List<IJsonObjectPairTreeNode> objectPairs = new List<IJsonObjectPairTreeNode>();
      while (this._lexer.Peek().Type != JsonTokenType.RCurly && this._lexer.Peek().Type != JsonTokenType.End)
      {
        objectPairs.Add(ReadPair());
        if (this._lexer.Peek().Type == JsonTokenType.Comma)
          ReadToken();
      }
      
      IJsonToken objectEndToken = ReadToken();
      if (objectEndToken.Type != JsonTokenType.RCurly)
        throw new TreeParseException("Expected Object end token");

      return new JsonObjectTreeNode(objectPairs);
    }

    private IJsonObjectPairTreeNode ReadPair()
    {
      IJsonToken pairKeyToken = ReadToken();

      if (pairKeyToken.Type != JsonTokenType.String)
        throw new TreeParseException("Expected string token for pair");

      IJsonTreeLeaf<string> pairKeyStringNode = new JsonValueTreeNode<string>((string)pairKeyToken.Value);

      IJsonToken pairColonDelimiter = ReadToken();
      if (pairColonDelimiter.Type != JsonTokenType.Colon)
        throw new TreeParseException("Expected colon");

      IJsonTreeItem pairKeyValueNode = ReadJson();
      if (pairKeyValueNode == null)
        throw new TreeParseException("Pair value node was null");

      return JsonObjectPairTreeNode.Create(pairKeyStringNode, pairKeyValueNode);
    }

    private IJsonTreeItem ReadArray()
    {
      IJsonToken arrayStartToken = ReadToken();
      if (arrayStartToken.Type != JsonTokenType.LBracket)
        throw new TreeParseException("Expected array start token");

      List<IJsonTreeItem> arrayItems = new List<IJsonTreeItem>();
      while (this._lexer.Peek().Type != JsonTokenType.RBracket && this._lexer.Peek().Type != JsonTokenType.End)
      {
        arrayItems.Add(ReadJson());
        if (this._lexer.Peek().Type == JsonTokenType.Comma)
          ReadToken();
      }
      
      IJsonToken arrayEndToken = ReadToken();
      if (arrayEndToken.Type != JsonTokenType.RBracket)
        throw new TreeParseException("Expected array end token");

      return new JsonArrayTreeNode(arrayItems);
    }

    

    private IJsonToken ReadToken()
    {
      IJsonToken token;
      if (!this._lexer.TryReadToken(out token))
        throw new TreeParseException("Could not read token from lexer");
      return token;
    }
  }

  public class TreeParseException : Exception
  {
    public TreeParseException(string message)
      : base(message)
    {

    }
  }
}
