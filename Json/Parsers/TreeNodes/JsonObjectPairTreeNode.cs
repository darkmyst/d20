using System;
using System.Collections.Generic;
using System.Linq;

namespace Json.Parsers.TreeNodes
{
  public interface IJsonObjectPairTreeNode : IJsonTreeNode
  {
    IJsonTreeLeaf<string> Key { get; }
    IJsonTreeItem Value { get; }
  }
  public interface IJsonObjectPairTreeNode<T> : IJsonTreeNode
    where T : IJsonTreeItem
  {
    IJsonTreeLeaf<string> Key { get; }
    T Value { get; }
  }

  public class JsonObjectPairTreeNode<T> : IJsonObjectPairTreeNode<T>, IJsonObjectPairTreeNode
    where T : IJsonTreeItem
  {
    public IJsonTreeLeaf<string> Key { get; internal set; }
    public T Value { get; internal set; }
    IJsonTreeItem IJsonObjectPairTreeNode.Value { get { return Value; } }

    public JsonObjectPairTreeNode(IJsonTreeLeaf<string> pairKeyStringNode, T pairKeyValueNode)
    {
      Key = pairKeyStringNode;
      Value = pairKeyValueNode;
    }

    public TreeItemType Type { get { return TreeItemType.Pair; } }

    IEnumerable<IJsonTreeItem> IJsonTreeNode.Children { get { return new IJsonTreeItem[] { Key, Value }; } }

    public override string ToString()
    {
      return string.Format(@"{0}: {1}", Key, Value.ToString());
    }

    public object Read(string jsonPath)
    {
      if (!jsonPath.StartsWith(Key.Value))
        throw new ArgumentException("jsonPath does not match this node");

      string childPath = jsonPath.Substring(Key.Value.Length);
      if (childPath[0] == '.')
        childPath = childPath.Substring(1);
      else if (Value is JsonArrayTreeNode)
        childPath = "this" + childPath;

      return Value.Read(childPath);
    }
  }

  public class JsonObjectPairTreeNode
  {
    private JsonObjectPairTreeNode() { }

    public static IJsonObjectPairTreeNode Create(IJsonTreeLeaf<string> keyNode, IJsonTreeItem valueNode)
    {
      Type classType = typeof(JsonObjectPairTreeNode<>);
      Type[] typeParams = new Type[] { valueNode.GetType() };
      Type constructedType = classType.MakeGenericType(typeParams);

      return (IJsonObjectPairTreeNode)Activator.CreateInstance(constructedType, keyNode, valueNode);
    }
  }
}