using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Json.Parsers.TreeNodes
{
  public class JsonArrayTreeNode : IJsonTreeNode
  {
    public JsonArrayTreeNode(IEnumerable<IJsonTreeItem> arrayItems)
    {
      Children = new List<IJsonTreeItem>(arrayItems ?? Enumerable.Empty<IJsonTreeItem>());
    }
    public JsonArrayTreeNode()
      : this(null)
    {

    }

    public TreeItemType Type { get { return TreeItemType.Array; } }
    public List<IJsonTreeItem> Children { get; internal set; }
    IEnumerable<IJsonTreeItem> IJsonTreeNode.Children { get { return Children; } }

    public override string ToString()
    {
      return string.Format("[ {0} ]", string.Join(", ", Children));
    }

    readonly Regex arrayPathRegex = new Regex(@"^(?:this|root)?(?:\[([1-9][0-9]*)\]\.?)?");
    public object Read(string jsonPath)
    {
      if (jsonPath == string.Empty)
        return this;

      var match = arrayPathRegex.Match(jsonPath);
      if (!match.Success)
        throw new ArgumentException("invalid array path");

      string childPath = jsonPath.Substring(match.Value.Length);

      string childIndexString = match.Groups[1].Value;
      if (childIndexString == string.Empty && childPath == string.Empty)
        return this;

      int childIndex = Convert.ToInt32(childIndexString);

      if (childIndex >= 0 && childIndex < Children.Count())
        return Children.ElementAt(childIndex).Read(childPath);

      throw new IndexOutOfRangeException("json array index is out of range");
    }
  }
}