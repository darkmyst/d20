﻿using Json.Tokenizers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Json.Parsers.TreeNodes
{
  public interface IJsonTreeItem
  {
    TreeItemType Type { get; }
    object Read(string jsonPath);
  }

  public interface IJsonTreeLeaf : IJsonTreeItem
  {
    object Value { get; }
  }

  public interface IJsonTreeLeaf<T> : IJsonTreeItem
  {
    T Value { get; }
  }

  public interface IJsonTreeNode : IJsonTreeItem
  {
    IEnumerable<IJsonTreeItem> Children { get; }
  }
}
