﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Json.Parsers.TreeNodes
{
  public enum TreeItemType
  {
    Value,
    Array,
    Pair,
    Object
  }
}
