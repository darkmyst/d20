using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Json.Parsers.TreeNodes
{
  public class JsonObjectTreeNode : IJsonTreeNode
  {
    public JsonObjectTreeNode(List<IJsonObjectPairTreeNode> objectPairs)
    {
      objectPairs = objectPairs ?? new List<IJsonObjectPairTreeNode>();
      if (objectPairs.Any(x => x.Type != TreeItemType.Pair))
        throw new ArgumentException("all children of an object tree node must be pair tree nodes");

      Children = objectPairs.ToDictionary(k => k.Key.Value, v => v.Value);
    }

    public JsonObjectTreeNode(Dictionary<string, IJsonTreeItem> objectPairs)
    {
      objectPairs = objectPairs ?? new Dictionary<string, IJsonTreeItem>();
      Children = objectPairs;
    }

    public JsonObjectTreeNode()
      : this((Dictionary<string, IJsonTreeItem>)null)
    {
    }

    public TreeItemType Type { get { return TreeItemType.Object; } }
    public Dictionary<string, IJsonTreeItem> Children { get; internal set; }
    IEnumerable<IJsonTreeItem> IJsonTreeNode.Children
    {
      get { return Children.Values; }
    }


    public override string ToString()
    {
      return string.Format("{{ {0} }}", string.Join(", ", Children.Select(x => JsonObjectPairTreeNode.Create(new JsonValueTreeNode<string>(x.Key), x.Value))));
    }

    readonly Regex readRegex = new Regex(@"^(?:(?:root|this)\[""(\w+)""\]|(\w+)\.?)");
    public object Read(string jsonPath)
    {
      if (jsonPath == string.Empty)
        return this;

      var match = readRegex.Match(jsonPath);
      if (!match.Success)
        throw new ArgumentException("invalid object key path");

      string childPath = jsonPath.Substring(match.Value.Length);

      string childKey = match.Groups[1].Value;
      if (string.IsNullOrWhiteSpace(childKey))
        childKey = match.Groups[2].Value;

      IJsonTreeItem node;
      if (!this.Children.TryGetValue(childKey, out node))
        throw new ArgumentException("unable to read path");

      if (childPath.StartsWith("["))
        childPath = "this" + childPath;

      return node.Read(childPath);
    }

  }
}