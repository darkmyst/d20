using System;
using System.Collections.Generic;

namespace Json.Parsers.TreeNodes
{
  public class JsonValueTreeNode<T> : IJsonTreeLeaf<T>, IJsonTreeLeaf
  {
    public JsonValueTreeNode(T value)
    {
      Value = value;
    }

    public T Value { get; internal set; }
    object IJsonTreeLeaf.Value
    {
      get { return this.Value; }
    }

    public TreeItemType Type { get { return TreeItemType.Value; } }

    public override string ToString()
    {
      if (typeof(T) == typeof(string))
        return string.Format(@"""{0}""", Value.ToString());
      else if (typeof(T) == typeof(bool?) && Value != null)
        return Value.ToString().ToLower();
      else if (typeof(T) == typeof(bool?) && Value == null)
        return "null";

      return Value.ToString();
    }

    public object Read(string jsonPath)
    {
      if (jsonPath != string.Empty)
        throw new ArgumentException("ValueTreeNode's don't have child paths, expected empty string for path");

      return Value;
    }
  }
}