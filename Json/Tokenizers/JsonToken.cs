﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Json.Tokenizers
{

  public interface IJsonToken
  {
    JsonTokenType Type { get; }
    object Value { get; }
  }
  
  public interface IToken<T>
  {
    JsonTokenType Type { get; }
    T Value { get; }
  }

  public class JsonToken 
  {
    private JsonToken() { }

    internal static IJsonToken Create(JsonTokenType tokenType)
    {
      return new Token<object>(null, tokenType);
    }   

    internal static IJsonToken Create<T>(T value, JsonTokenType tokenType)
    {
      return new Token<T>(value, tokenType);
    }
  }

  [DebuggerDisplay("{Type}:{Value}")]
  public class Token<T> : IJsonToken, IToken<T>
  {
    
    public static readonly Token<object> Empty = new Token<object>(null, JsonTokenType.Empty);

    public JsonTokenType Type { get; protected set; }
    public T Value { get; protected set; }
    object IJsonToken.Value
    {
      get { return this.Value; }
    }

    internal Token(T value, JsonTokenType type)
    {
      Type = type;
      Value = value;
    }

    public override bool Equals(object obj)
    {
      if (obj == null) return false;
      
      IJsonToken other = (IJsonToken)obj;
      if (!other.Value.Equals(this.Value) || !other.Type.Equals(this.Type))
        return false;

      return true;
    }

    public override int GetHashCode()
    {
      int hashCode = 0;
      
      if (this.Value != null) hashCode += this.Value.GetHashCode() * 5;
      hashCode += this.Type.GetHashCode() * 10;
      return hashCode;
    }   
  }

  public enum JsonTokenType
  {
    Empty,
    String,
    Number,
    Bool,
    LBracket,
    RBracket,
    LCurly,
    RCurly,
    Colon,
    Comma,
    End,
  }
}
