﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Json.Tokenizers.TokenReaders
{
  internal class NumberTokenReader : ITokenReader
  {
    Regex regex = new Regex(@"\A(-? (?= [1-9]|0(?!\d) ) \d+ (\.\d+)? ([eE] [+-]? \d+)? )", RegexOptions.IgnorePatternWhitespace);
    public bool TryReadToken(string input, out string matchedValue, out IJsonToken token)
    {

      var match = regex.Match(input);
      if (match.Success)
      {
        matchedValue = match.Value;
        token = new Token<double>(Convert.ToDouble(match.Groups[1].Value), JsonTokenType.Number);
        return true;
      }
      
      matchedValue = null; token = null;
      return false;
    }
  }
}
