﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Json.Tokenizers.TokenReaders
{
  internal class BoolTokenReader : ITokenReader
  {
    Regex regex = new Regex(@"\A(true|false|null)");
    public bool TryReadToken(string input, out string matchedValue, out IJsonToken token)
    {
      var match = regex.Match(input);
      if (match.Success)
      {
        matchedValue = match.Value;
        bool? tokenValue = null;
        switch (match.Groups[1].Value)
        {
          case "true": tokenValue = true; break;
          case "false": tokenValue = false; break;
          default: tokenValue = null; break;
        }
        token = new Token<bool?>(tokenValue, JsonTokenType.Bool);
        return true;
      }
      
      matchedValue = null; token = null;
      return false;
    }
  }

}
