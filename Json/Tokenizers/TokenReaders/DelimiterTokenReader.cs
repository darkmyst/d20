﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Json.Tokenizers.TokenReaders
{
  internal class DelimiterTokenReader : ITokenReader
  {
    Regex regex = new Regex(@"\A([\[\]\{\},:])");
    public bool TryReadToken(string input, out string matchedValue, out IJsonToken token)
    {
      var match = regex.Match(input);
      if (match.Success)
      {
        matchedValue = match.Value;
        JsonTokenType tokenType = JsonTokenType.Empty;
        switch (match.Groups[1].Value)
        {
          case "[": tokenType = JsonTokenType.LBracket; break;
          case "]": tokenType = JsonTokenType.RBracket; break;
          case "{": tokenType = JsonTokenType.LCurly; break;
          case "}": tokenType = JsonTokenType.RCurly; break;
          case ":": tokenType = JsonTokenType.Colon; break;
          case ",": tokenType = JsonTokenType.Comma; break;
          default: throw new InvalidOperationException("DelimiterTokenReader regex false match!"); 
        }
        token = new Token<string>(match.Groups[1].Value, tokenType);
        return true;
      }
      
      matchedValue = null; token = null;
      return false;
    }
  }

}
