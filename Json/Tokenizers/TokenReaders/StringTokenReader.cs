﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Json.Tokenizers.TokenReaders
{
  internal class StringTokenReader : ITokenReader
  {
    Regex stringRegex = new Regex(@"\A""((?:[^""\\]|\\[""\\/bfnrt]|\\u[0-9A-Fa-f]{4})*)""");
    public bool TryReadToken(string input, out string matchedValue, out IJsonToken token)
    {

      var match = stringRegex.Match(input);
      if (match.Success)
      {
        matchedValue = match.Value;
        token = new Token<string>(match.Groups[1].Value, JsonTokenType.String);
        return true;
      }
      
      matchedValue = null; token = null;
      return false;
    }
  }
}
