﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Json.Tokenizers.TokenReaders
{
  internal interface ITokenReader
  {
    bool TryReadToken(string input, out string matchedValue, out IJsonToken token);
  }
}
