﻿using Json.Tokenizers.TokenReaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Json.Tokenizers
{
  public class JsonTokenizer
  {
    string _input;
    Stack<IJsonToken> _previousTokens = new Stack<IJsonToken>();
    Stack<IJsonToken> _nextTokens = new Stack<IJsonToken>();
    List<ITokenReader> _tokenReaders = new List<ITokenReader>
      {
        new DelimiterTokenReader(),
        new StringTokenReader(),
        new BoolTokenReader(),
        new NumberTokenReader(),
      };


    public JsonTokenizer(string input)
    {
      this._input = input;
    }

    public bool TryReadToken(out IJsonToken token)
    {
      if (this._nextTokens.Count > 0)
      {
        this._previousTokens.Push(this._nextTokens.Pop());
        token = this._previousTokens.Peek();
        return token.Type != JsonTokenType.End;
      }

      if (string.IsNullOrWhiteSpace(this._input))
      {
        if (_previousTokens.Count == 0 || _previousTokens.Peek().Type != JsonTokenType.End)
          ProcessToken(JsonToken.Create(JsonTokenType.End), "");

        token = this._previousTokens.Peek();
        return token.Type != JsonTokenType.End;
      }

      string matchedValue = null;
      this._input = this._input.Trim();

      foreach (var tokenReader in _tokenReaders)
      {
        if (tokenReader.TryReadToken(this._input, out matchedValue, out token))
        {
          ProcessToken(token, matchedValue);
          return token.Type != JsonTokenType.End;
        }
      }

      throw new ArgumentException("unrecognized token");
    }

    private void ProcessToken(IJsonToken token, string matchedValue)
    {
      this._input = this._input.Substring(matchedValue.Length);
      this._previousTokens.Push(token);
    }

    public void Revert()
    {
      this._nextTokens.Push(this._previousTokens.Pop());
    }

    public IJsonToken Peek()
    {
      IJsonToken token;

      if (TryReadToken(out token))
        this.Revert();

      return token;
    }
  }

}
